absl-py==0.12.0
aiofiles @ file:///home/conda/feedstock_root/build_artifacts/aiofiles_1603888855375/work
alabaster==0.7.12
appdirs @ file:///home/conda/feedstock_root/build_artifacts/appdirs_1603108395799/work
appnope @ file:///opt/concourse/worker/volumes/live/5f13e5b3-5355-4541-5fc3-f08850c73cf9/volume/appnope_1606859448618/work
argon2-cffi @ file:///opt/concourse/worker/volumes/live/d733ceb5-7f19-407b-7da7-a386540ab855/volume/argon2-cffi_1613037492998/work
astunparse==1.6.3
async-generator @ file:///home/ktietz/src/ci/async_generator_1611927993394/work
attrs @ file:///tmp/build/80754af9/attrs_1604765588209/work
audioread @ file:///Users/runner/miniforge3/conda-bld/audioread_1607457517152/work
autopep8 @ file:///tmp/build/80754af9/autopep8_1615918855173/work
Babel==2.9.1
backcall @ file:///home/ktietz/src/ci/backcall_1611930011877/work
bleach @ file:///tmp/build/80754af9/bleach_1612211392645/work
brotlipy==0.7.0
cachetools==4.2.1
certifi==2020.12.5
cffi @ file:///opt/concourse/worker/volumes/live/0ef369cc-6ba0-47e7-75da-208c6400381d/volume/cffi_1613246948181/work
chardet @ file:///opt/concourse/worker/volumes/live/c798b2ee-88b1-4341-6830-161a92c2399e/volume/chardet_1607706832595/work
colorama==0.4.4
conda==4.10.1
conda-package-handling @ file:///opt/concourse/worker/volumes/live/73497069-9b43-4ad9-50ec-1abb340e14eb/volume/conda-package-handling_1618262140058/work
cryptography @ file:///opt/concourse/worker/volumes/live/c515855a-effc-46df-74dc-542901b701da/volume/cryptography_1616769282442/work
cycler==0.10.0
decorator @ file:///home/conda/feedstock_root/build_artifacts/decorator_1618380339289/work
defusedxml @ file:///tmp/build/80754af9/defusedxml_1615228127516/work
dnspython==1.16.0
docopt==0.6.2
docutils==0.16
entrypoints==0.3
fett==0.3.2
findspark==1.4.2
flatbuffers==1.12
future==0.18.2
gast==0.3.3
google-auth==1.29.0
google-auth-oauthlib==0.4.4
google-pasta==0.2.0
grpcio==1.32.0
h11 @ file:///home/conda/feedstock_root/build_artifacts/h11_1609518832927/work
h2 @ file:///Users/runner/miniforge3/conda-bld/h2_1610644033925/work
h5py==2.10.0
hpack==4.0.0
httpcore @ file:///home/conda/feedstock_root/build_artifacts/httpcore_1611864786085/work
httptools @ file:///Users/runner/miniforge3/conda-bld/httptools_1619271984375/work
httpx @ file:///home/conda/feedstock_root/build_artifacts/httpx_1615806049495/work
humanize @ file:///tmp/build/80754af9/humanize_1618263084678/work
hyperframe @ file:///home/conda/feedstock_root/build_artifacts/hyperframe_1619110129307/work
idna @ file:///home/linux1/recipes/ci/idna_1610986105248/work
imagesize==1.2.0
importlib-metadata @ file:///opt/concourse/worker/volumes/live/a634a87c-b5e5-41bd-628d-cd0413666c93/volume/importlib-metadata_1617877368300/work
ipykernel @ file:///opt/concourse/worker/volumes/live/88f541d3-5a27-498f-7391-f2e50ca36560/volume/ipykernel_1596206680118/work/dist/ipykernel-5.3.4-py3-none-any.whl
ipython @ file:///opt/concourse/worker/volumes/live/c432d8a7-d8f3-4e24-590f-f03d7e5f35e1/volume/ipython_1617120884257/work
ipython-genutils @ file:///tmp/build/80754af9/ipython_genutils_1606773439826/work
ipywidgets @ file:///tmp/build/80754af9/ipywidgets_1610481889018/work
jedi==0.17.0
Jinja2 @ file:///tmp/build/80754af9/jinja2_1612213139570/work
joblib @ file:///tmp/build/80754af9/joblib_1613502643832/work
jsonschema @ file:///tmp/build/80754af9/jsonschema_1602607155483/work
jupyter==1.0.0
jupyter-client @ file:///tmp/build/80754af9/jupyter_client_1616770841739/work
jupyter-console @ file:///tmp/build/80754af9/jupyter_console_1616615302928/work
jupyter-core @ file:///opt/concourse/worker/volumes/live/c8df8dce-dbb3-46e7-649c-adf4ed2dd00a/volume/jupyter_core_1612213293829/work
jupyterlab-pygments @ file:///tmp/build/80754af9/jupyterlab_pygments_1601490720602/work
jupyterlab-widgets @ file:///tmp/build/80754af9/jupyterlab_widgets_1609884341231/work
Keras==2.3.1
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.2
kiwisolver @ file:///opt/concourse/worker/volumes/live/0b2f3e77-eaa3-4995-7dd0-c994762fcbde/volume/kiwisolver_1612282417472/work
librosa @ file:///home/conda/feedstock_root/build_artifacts/librosa_1595413403988/work
livereload==2.6.3
llvmlite==0.36.0
Markdown==3.3.4
MarkupSafe @ file:///opt/concourse/worker/volumes/live/cb778296-98db-45ad-411e-6f726e102dc3/volume/markupsafe_1594371638608/work
matplotlib @ file:///opt/concourse/worker/volumes/live/41e8cd50-031f-4dda-5787-dd3c4f4e0f08/volume/matplotlib-suite_1613407855571/work
mistune @ file:///opt/concourse/worker/volumes/live/95802d64-d39c-491b-74ce-b9326880ca54/volume/mistune_1594373201816/work
mkl-fft==1.3.0
mkl-random @ file:///opt/concourse/worker/volumes/live/54b31a45-1da5-4512-5c3a-93c9ff2af8bc/volume/mkl_random_1618853970587/work
mkl-service==2.3.0
multidict @ file:///Users/runner/miniforge3/conda-bld/multidict_1610319065069/work
nbclient @ file:///tmp/build/80754af9/nbclient_1614364831625/work
nbconvert @ file:///opt/concourse/worker/volumes/live/2b9c1d93-d0fd-432f-7d93-66c93d81b614/volume/nbconvert_1601914875037/work
nbformat @ file:///tmp/build/80754af9/nbformat_1617383369282/work
nest-asyncio @ file:///tmp/build/80754af9/nest-asyncio_1613680548246/work
networkx==2.5.1
notebook @ file:///opt/concourse/worker/volumes/live/06e4d8fc-70ed-4cb0-554c-65c20061cb7f/volume/notebook_1616443453928/work
numba==0.53.1
numpy @ file:///opt/concourse/worker/volumes/live/84716fc3-63c0-4ad6-5a23-37f5ad6b3bfa/volume/numpy_and_numpy_base_1618497240837/work
oauthlib==3.1.0
olefile==0.46
opt-einsum==3.3.0
packaging @ file:///tmp/build/80754af9/packaging_1611952188834/work
pandas==1.2.4
pandocfilters @ file:///opt/concourse/worker/volumes/live/c330e404-216d-466b-5327-8ce8fe854d3a/volume/pandocfilters_1605120442288/work
parso @ file:///tmp/build/80754af9/parso_1617223946239/work
pexpect @ file:///tmp/build/80754af9/pexpect_1605563209008/work
pickleshare @ file:///tmp/build/80754af9/pickleshare_1606932040724/work
Pillow @ file:///opt/concourse/worker/volumes/live/ca23594b-6e35-4c8c-5637-50ac0b550473/volume/pillow_1617386168018/work
pooch @ file:///home/conda/feedstock_root/build_artifacts/pooch_1606467285986/work
prometheus-client @ file:///tmp/build/80754af9/prometheus_client_1618088486455/work
prompt-toolkit @ file:///tmp/build/80754af9/prompt-toolkit_1616415428029/work
protobuf==3.15.8
psutil @ file:///opt/concourse/worker/volumes/live/0673cd4b-30c1-4470-7490-d8955610f5d5/volume/psutil_1612298002202/work
psycopg2-binary==2.9.1
ptyprocess @ file:///tmp/build/80754af9/ptyprocess_1609355006118/work/dist/ptyprocess-0.7.0-py2.py3-none-any.whl
pyasn1==0.4.8
pyasn1-modules==0.2.8
PyAudio==0.2.11
pycodestyle @ file:///tmp/build/80754af9/pycodestyle_1615748559966/work
pycosat==0.6.3
pycparser @ file:///tmp/build/80754af9/pycparser_1594388511720/work
pydub==0.24.1
Pygments @ file:///tmp/build/80754af9/pygments_1615143339740/work
pymongo==3.11.3
pyOpenSSL @ file:///tmp/build/80754af9/pyopenssl_1608057966937/work
pyparsing @ file:///home/linux1/recipes/ci/pyparsing_1610983426697/work
pyrsistent @ file:///opt/concourse/worker/volumes/live/ff11f3f0-615b-4508-471d-4d9f19fa6657/volume/pyrsistent_1600141727281/work
PySocks @ file:///opt/concourse/worker/volumes/live/85a5b906-0e08-41d9-6f59-084cee4e9492/volume/pysocks_1594394636991/work
python-dateutil @ file:///home/ktietz/src/ci/python-dateutil_1611928101742/work
python-dotenv==0.18.0
python-jsonrpc-server==0.3.4
pytz @ file:///tmp/build/80754af9/pytz_1612215392582/work
PyYAML==5.4.1
pyzmq==20.0.0
qtconsole @ file:///tmp/build/80754af9/qtconsole_1616775094278/work
QtPy==1.9.0
requests @ file:///tmp/build/80754af9/requests_1608241421344/work
requests-oauthlib==1.3.0
resampy==0.2.2
rfc3986 @ file:///home/conda/feedstock_root/build_artifacts/rfc3986_1593217945888/work
rsa==4.7.2
rstcheck==3.3.1
ruamel-yaml-conda @ file:///opt/concourse/worker/volumes/live/53b096c9-f5b7-4029-7f1b-056927554e08/volume/ruamel_yaml_1616016691174/work
sanic @ file:///Users/runner/miniforge3/conda-bld/sanic_1615411315677/work
scikit-learn @ file:///opt/concourse/worker/volumes/live/0c77b068-a026-4996-6842-33e5544ec13b/volume/scikit-learn_1614446667823/work
scipy @ file:///opt/concourse/worker/volumes/live/7d10d993-3825-404e-6e5d-9947c19e8c6d/volume/scipy_1618855951189/work
seaborn==0.11.1
Send2Trash @ file:///tmp/build/80754af9/send2trash_1607525499227/work
six @ file:///opt/concourse/worker/volumes/live/5b31cb27-1e37-4ca5-6e9f-86246eb206d2/volume/six_1605205320872/work
sklearn==0.0
sniffio @ file:///Users/runner/miniforge3/conda-bld/sniffio_1610318396434/work
snooty-lextudio==1.9.1.dev0
snowballstemmer==2.1.0
SoundFile @ file:///home/conda/feedstock_root/build_artifacts/pysoundfile_1607011569782/work
Sphinx==3.5.4
sphinx-autobuild==2021.3.14
sphinxcontrib-applehelp==1.0.2
sphinxcontrib-devhelp==1.0.2
sphinxcontrib-htmlhelp==1.0.3
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.3
sphinxcontrib-serializinghtml==1.1.4
tensorboard==2.5.0
tensorboard-data-server==0.6.0
tensorboard-plugin-wit==1.8.0
tensorflow==2.4.1
tensorflow-estimator==2.4.0
termcolor==1.1.0
terminado==0.9.4
testpath @ file:///home/ktietz/src/ci/testpath_1611930608132/work
threadpoolctl @ file:///tmp/tmp9twdgx9k/threadpoolctl-2.1.0-py3-none-any.whl
toml @ file:///tmp/build/80754af9/toml_1616166611790/work
torch==1.5.0
torchvision==0.6.0
tornado @ file:///opt/concourse/worker/volumes/live/05341796-4198-4ded-4a9a-332fde3cdfd1/volume/tornado_1606942323372/work
tqdm @ file:///tmp/build/80754af9/tqdm_1615925068909/work
traitlets @ file:///home/ktietz/src/ci/traitlets_1611929699868/work
typing-extensions==3.7.4.3
ujson==1.35
urllib3 @ file:///tmp/build/80754af9/urllib3_1615837158687/work
uvloop==0.14.0
watchdog==1.0.2
wcwidth @ file:///tmp/build/80754af9/wcwidth_1593447189090/work
webencodings==0.5.1
websockets @ file:///Users/runner/miniforge3/conda-bld/websockets_1610127500360/work
Werkzeug==1.0.1
widgetsnbextension==3.5.1
wrapt==1.12.1
zipp @ file:///tmp/build/80754af9/zipp_1615904174917/work

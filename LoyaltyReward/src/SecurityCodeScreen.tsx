import {useNavigation} from "@react-navigation/native";
import React,{useEffect,useState} from "react";
import {SafeAreaView,View,ScrollView,Text,TextInput,TouchableOpacity,TouchableWithoutFeedback,Keyboard,KeyboardAvoidingView,
Platform,ImageBackground} from "react-native";
import {useDispatch,useSelector} from "react-redux";
import {register,registerNull, resendSecurityCode, verifySecurityCode} from "./redux/user/thunks";
import {IRootState} from "../store";
import {backgroundStyles,flexStyles} from "./styles/LoadingScreenStyles";
import {styles} from "./styles/SecurityCodeScreenStyles";

// function checkIsNumber(anyDigit:string){
// 	if(isNaN(parseInt(anyDigit))){
// 		return false;
// 	}else{
// 		return true;
// 	}
// }

function SecurityCodeScreen(){
	const [firstDigit, setFirstDigit] = useState("");
	const [secondDigit, setSecondDigit] = useState("");
	const [thirdDigit, setThirdDigit] = useState("");
	const [fourthDigit, setFourthDigit] = useState("");
	const [fifthDigit, setFifthDigit] = useState("");
	const [sixthDigit, setSixthDigit] = useState("");
	const [isResent, setIsResent] = useState(false);
	const [isWrongCode, setIsWrongCode] = useState(false);
	const userId = useSelector((state:IRootState)=>state.user.userId);
	const verifySecurityBooleanResult = useSelector((state:IRootState)=>state.user.verifySecurityCodeResult.success);
	// console.log(isResent)
	// console.log(isWrongCode)

	// console.log(wrongPhone);
	// console.log(wrongEmail);
	const navigation = useNavigation();
	const dispatch = useDispatch();
	// let isRegistered = useSelector((state:IRootState)=>state.user.isRegistered);

	return(
		<KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={flexStyles.flex}>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
				<ScrollView style={styles.main} keyboardShouldPersistTaps="handled">
					<ImageBackground source={require("../images/simpleBackground.png")} style={backgroundStyles.image}>
						<SafeAreaView style={flexStyles.flex}>
							{/* <CustomerRoleCheckBox/> */}
							<View style={styles.register}>
								<Text style={styles.title}>用戶驗證</Text>
							</View>
							<View style={styles.securityCodeContainer}>
								<View style={styles.securityCodeBox}>
									<TextInput
										style={styles.securityCodeText} onChangeText={setFirstDigit}
										value={firstDigit} maxLength={1} keyboardType="numeric"
										textContentType="oneTimeCode">
									</TextInput>
								</View>
								<View style={styles.securityCodeBox}>
									<TextInput
										style={styles.securityCodeText} onChangeText={setSecondDigit}
										// onLayout={event=>event.currentTarget}
										value={secondDigit} maxLength={1} keyboardType="numeric"

										textContentType="oneTimeCode">
									</TextInput>
								</View>
								<View style={styles.securityCodeBox}>
									<TextInput
										style={styles.securityCodeText} onChangeText={setThirdDigit}
										value={thirdDigit} maxLength={1} keyboardType="numeric"
										textContentType="oneTimeCode">
									</TextInput>
								</View>
								<View style={styles.securityCodeBox}>
									<TextInput
										style={styles.securityCodeText} onChangeText={setFourthDigit}
										value={fourthDigit} maxLength={1} keyboardType="numeric"
										textContentType="oneTimeCode">
									</TextInput>
								</View>
								<View style={styles.securityCodeBox}>
									<TextInput
										style={styles.securityCodeText} onChangeText={setFifthDigit}
										value={fifthDigit} maxLength={1} keyboardType="numeric"
										textContentType="oneTimeCode">
									</TextInput>
								</View>
								<View style={styles.securityCodeBoxMargin}>
									<TextInput
										style={styles.securityCodeTextMargin} onChangeText={setSixthDigit}
										value={sixthDigit} maxLength={1} keyboardType="numeric"
										textContentType="oneTimeCode">
									</TextInput>
								</View>
							</View>
							<View style={styles.registerResendContainer}>
								<Text style={styles.resendText}>請輸入已發送到你電郵的驗證碼</Text>
								<Text style={styles.resendText}>如果你仍未收到驗證碼,請按重新發送</Text>
							</View>
							{(!isResent && !isWrongCode) && <View style={styles.blankContainer}></View>}
							{isResent === true && (
							<View style={styles.dangerNotification}>
								<Text style={styles.dangerNotificationText}>驗證碼已經重新發送,請查看你的電郵</Text>
							</View>)}
							{isWrongCode === true && (
							<View style={styles.dangerNotification}>
								<Text style={styles.dangerNotificationText}>你的驗證碼不正確,請重新輸入</Text>
							</View>)}
							<View style={styles.registerContainer}>
								<TouchableOpacity onPress={() => {
									dispatch(resendSecurityCode(userId));
									setIsResent(true);
								}}>
									<Text style={styles.button}>重新發送</Text>
								</TouchableOpacity>
							</View>
							<View style={styles.resendContainer}>
								<TouchableOpacity style={styles.confirmSubmitButton} onPress={() => {
									dispatch(verifySecurityCode(userId,firstDigit+secondDigit+thirdDigit
									+fourthDigit+fifthDigit+sixthDigit));
									if(verifySecurityBooleanResult === true){ // value of true
										// navigate to where? get access to the real content!
										navigation.navigate("RoleLogin");
										// console.log("correct code");
										setIsResent(false);
										setIsWrongCode(false);
									}else if(verifySecurityBooleanResult === false){
										// console.log("wrong code");
										setIsWrongCode(true);
									}
								}}>
									<Text style={styles.buttonText}>確定</Text>
								</TouchableOpacity>
								<TouchableOpacity style={styles.submitButton} onPress={() => {
									// navigate to where?
									navigation.navigate("Register",{screen:"RegisterScreen"});
									setIsResent(false);
									setIsWrongCode(false);
								}}>
									<Text style={styles.button}>取消</Text>
								</TouchableOpacity>
							</View>
						</SafeAreaView>
					</ImageBackground>
				</ScrollView>
			</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	)
}

export default SecurityCodeScreen;
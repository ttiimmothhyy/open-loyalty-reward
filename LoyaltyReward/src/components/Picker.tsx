import React,{useState} from "react";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import {PickerItems} from "../../models";
import RNPickerSelect from "react-native-picker-select";
import {longPickerSelectStyles,middlePickerSelectStyles,pickerSelectStyles,middlePickerSelectGenderStyles}
from "../styles/PickerStyles";
import {useDispatch} from "react-redux";
import {Dispatch} from "redux";
import {setTimeslotNull} from "../redux/availableAppointments/actions";
import {setTimeslotNull as agentSetTimeslotNull} from "../redux/agentTreatment/actions";
import {setPurchaseNull} from "../redux/treatmentPurchases/actions";
import {changeNoLocationToNull, noEnoughPointToFalse} from "../redux/redeem/actions";

export const Picker:React.FC<{label:string,items:PickerItems[]}> = ({label,items}) => {
	const placeholder = {
		label:label,
		value:null,
		color:'#555555',
	}
	const [value,setValue] = useState(placeholder);

	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={value => setValue(value)}
			items={items}
			InputAccessoryView={() => null}
			style={pickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const MiddlePicker:React.FC<{label:string,items:PickerItems[]}> = ({label,items}) => {
	const placeholder = {
		label:label,
		value:null,
		color:'#555555',
	}
	const [value,setValue] = useState(placeholder);
	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={value => setValue(value)}
			items={items}
			InputAccessoryView={() => null}
			style={middlePickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const MiddleCriteriaPicker:React.FC<{defaultValue:string,label:string,items:PickerItems[],criteria:React.Dispatch<any>,
reactDispatch:((dispatch: Dispatch<any>) => Promise<void>)|null,nearCriteria:number|null}> =
({defaultValue,label,items,criteria,reactDispatch,nearCriteria}) => {
	const dispatch = useDispatch();
	const placeholder = {
		label:defaultValue ? defaultValue : label,
		value:defaultValue ? items.filter(item=>item.label === defaultValue).map(item=>item.value) : null,
		color:defaultValue ? "#000000" : "#555555"
	}
	const [value,setValue] = useState(placeholder);
	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={(value) => {
				setValue(value)
				criteria(value)
				{!value && !nearCriteria && reactDispatch && dispatch(reactDispatch)}
				// console.log(value)
			}}
			items={items}
			InputAccessoryView={() => null}
			style={middlePickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const MiddleCriteriaPickerForGender:React.FC<{defaultValue:string,label:string,items:PickerItems[],
criteria:React.Dispatch<any>,reactDispatch:((dispatch: Dispatch<any>) => Promise<void>)|null}> =
({defaultValue,label,items,criteria,reactDispatch}) => {
	const dispatch = useDispatch();
	const placeholder = {
		label:defaultValue ? "" : label,
		value:defaultValue ? items.filter(item=>item.label === defaultValue).map(item=>item.value) : null,
		color:defaultValue ? "#000000" : "#555555"
	}
	const [value,setValue] = useState(placeholder);
	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={(value) => {
				setValue(value)
				criteria(value)
				{!value && reactDispatch && dispatch(reactDispatch)}
				// console.log((genderList.filter(item=>item.value === value).map(item=>item.label))[0])
			}}
			items={items}
			InputAccessoryView={() => null}
			style={middlePickerSelectGenderStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const MiddlePickUpLocationCriteriaPicker:React.FC<{defaultValue:string,label:string,items:PickerItems[],criteria:React.Dispatch<any>,
reactDispatch:((dispatch: Dispatch<any>) => Promise<void>)|null,nearCriteria:number|null}> =
({defaultValue,label,items,criteria,reactDispatch,nearCriteria}) => {
	const dispatch = useDispatch();
	const placeholder = {
		label:defaultValue ? defaultValue : label,
		value:defaultValue ? items.filter(item=>item.label === defaultValue).map(item=>item.value) : null,
		color:defaultValue ? "#000000" : "#555555"
	}
	const [value,setValue] = useState(placeholder);
	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={(value) => {
				setValue(value)
				criteria(value)
				{!value && !nearCriteria && reactDispatch && dispatch(reactDispatch)}
				dispatch(changeNoLocationToNull())
				dispatch(noEnoughPointToFalse())
				// console.log(value)
			}}
			items={items}
			InputAccessoryView={() => null}
			style={middlePickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const LongCriteriaPicker:React.FC<{label:string,items:PickerItems[],criteria:React.Dispatch<any>,
reactDispatch:((dispatch: Dispatch<any>) => Promise<void>)|null}> =
({label,items,criteria,reactDispatch}) => {
		const dispatch = useDispatch();
		const placeholder = {
			label:label,
			value:null,
			color:"#555555"
		}
		const [value,setValue] = useState(placeholder);
		return(
			<RNPickerSelect
				placeholder={placeholder}
				onValueChange={(value) => {
					setValue(value)
					criteria(value)
					{!value && reactDispatch && dispatch(reactDispatch)}
					// console.log(value)
				}}
				items={items}
				InputAccessoryView={() => null}
				style={longPickerSelectStyles}
				Icon={() => {
					return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
				}}
				value={value}
				useNativeAndroidPickerStyle={false}
			/>
		)
	}

export const LongPicker:React.FC<{label:string,items:PickerItems[]}> = ({label,items}) => {
	const placeholder = {
		label:label,
		value:null,
		color:'#555555',
	}
	const [value,setValue] = useState(placeholder);

	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={value => setValue(value)}
			items={items}
			InputAccessoryView={() => null}
			style={longPickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const LongAppointmentPicker:React.FC<{label:string,items:PickerItems[],criteria:React.Dispatch<any>}> =
({label,items,criteria}) => {
	const placeholder = {
		label:label,
		value:null,
		color:'#555555',
	}
	const [value,setValue] = useState(placeholder);
	const dispatch = useDispatch();

	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={(value) => {
				setValue(value)
				criteria(value)
				{value && dispatch(setTimeslotNull())}
			}}
			items={items}
			InputAccessoryView={() => null}
			style={longPickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const LongAgentAppointmentPicker:React.FC<{label:string,items:PickerItems[],criteria:React.Dispatch<any>}> =
({label,items,criteria}) => {
	const placeholder = {
		label:label,
		value:null,
		color:'#555555',
	}
	const [value,setValue] = useState(placeholder);
	const dispatch = useDispatch();

	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={(value) => {
				setValue(value)
				criteria(value)
				{value && dispatch(agentSetTimeslotNull())}
			}}
			items={items}
			InputAccessoryView={() => null}
			style={longPickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}

export const LongAppointmentPurchasePicker:React.FC<{label:string,items:PickerItems[],criteria:React.Dispatch<any>}> =
({label,items,criteria}) => {
	const placeholder = {
		label:label,
		value:null,
		color:'#555555',
	}
	const [value,setValue] = useState(placeholder);
	const dispatch = useDispatch();

	return(
		<RNPickerSelect
			placeholder={placeholder}
			onValueChange={(value) => {
				setValue(value)
				criteria(value)
				{value && dispatch(setPurchaseNull())}
			}}
			items={items}
			InputAccessoryView={() => null}
			style={longPickerSelectStyles}
			Icon={() => {
				return <FontAwesome name="angle-down" size={16} color="#adadad"/>;
			}}
			value={value}
			useNativeAndroidPickerStyle={false}
		/>
	)
}
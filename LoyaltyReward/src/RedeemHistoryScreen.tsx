import {useNavigation} from "@react-navigation/native";
import React,{useEffect} from "react";
import {ScrollView,TouchableOpacity,useColorScheme,View,Text} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import {IRootState} from "../store";
import {getPointsRedeemedHistory} from "./redux/point/thunks";
import {styles} from "./styles/RedeemHistoryScreenStyles";

export const PointsRedeemedHistoryRow:React.FC<{order_ref:string,date:String,location:string,points:String}> = ({order_ref,date,location,points}) => {
	const navigation = useNavigation();
	return(
		<TouchableOpacity style={styles.content} onPress={() => {
			navigation.navigate("RedeemHistoryDetailsScreen",{
				order_ref:order_ref
			})
		}}>
			<View style={styles.year}><Text>{date.slice(0,16)}</Text></View>
			<View style={styles.middleText}><Text>{location}</Text></View>
			<View style={styles.finalText}><Text>-{points}分</Text></View>
		</TouchableOpacity>
	)
}

function RedeemHistoryScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const dispatch = useDispatch();
	const pointsRedeemedHistoryRows = useSelector((state:IRootState)=>state.pointPage.pointsRedeemedHistoryRow);

	useEffect(() => {
		dispatch(getPointsRedeemedHistory());
	},[dispatch])

	return(
		<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.historyContainer}>
				<ScrollView style={styles.historyBorder}>
					<View style={styles.titleContent}>
						<Text style={styles.titleText}>時間</Text>
						<Text style={styles.middleTitleText}>換領編號</Text>
						<View style={styles.finalTitleTextContainer}>
							<Text style={styles.titleText}>使用積分</Text>
						</View>
					</View>
					{pointsRedeemedHistoryRows.length > 0 && pointsRedeemedHistoryRows.map((pointsRedeemedHistoryRow,index)=>(
						<PointsRedeemedHistoryRow key={index} order_ref={pointsRedeemedHistoryRow.order_ref} date={pointsRedeemedHistoryRow.date}
						// name={pointsRedeemedHistoryRow}
						location={pointsRedeemedHistoryRow.location} points={pointsRedeemedHistoryRow.points}/>
					))}
				</ScrollView>
			</View>
		</View>
	)
}

export default RedeemHistoryScreen;
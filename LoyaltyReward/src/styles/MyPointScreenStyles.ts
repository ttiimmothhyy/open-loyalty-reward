import {StyleSheet,Dimensions} from "react-native";

export const styles = StyleSheet.create({
	flexContainer:{
		height:Dimensions.get("window").height
	},
	titleContainer:{
		flexDirection:"row",
		alignItems:"center",
	},
	titleRowContainer:{
		width:Dimensions.get("window").width * 0.6,
		marginVertical:20,
		flexDirection:"row",
		justifyContent:"flex-end",
	},
	titleFirstContainer:{
		paddingRight:10,
		alignItems:"center",
	},
	pointIconContainer:{
		height:40,
		width:40,
		borderRadius:80,
		backgroundColor:"#cccccc",
		alignItems:"center",
		justifyContent:"center",
		marginRight:10
	},
	pointIcon:{
		fontSize:28,
		color:"#ffffff"
	},
	titleGreyText:{
		color:"#adadad",
		fontSize:25,
		marginBottom:5,
		fontWeight:"600"
	},
	titleText:{
		fontSize:25,
		fontWeight:"600",
		marginBottom:5,
	},
	newTitleText:{
		fontSize:25,
		fontWeight:"600",
	},
	contentContainer:{
		marginHorizontal:25,
		marginBottom:10
	},
	expiredContentContainer:{
		marginLeft:25,
		marginBottom:10,
		marginRight:35
	},
	pointTitle:{
		color:"#adadad",
		fontSize:20,
		marginBottom:10,
	},
	content:{
		flexDirection:"row",
		marginBottom:3,
		// justifyContent:"space-between"
	},
	expiredContent:{
		flexDirection:"row",
		marginBottom:3,
		justifyContent:"space-between"
	},
	middleText:{
		width:Dimensions.get("window").width * 0.36,
	},
	finalText:{
		width:55,
		alignItems:"flex-end",
	},
	dateText:{
		width:Dimensions.get("window").width * 0.29,
		flexDirection:"row",
		justifyContent:"space-between",
		alignItems:"center",
		marginRight:20
	},
	expiredDateText:{
		width:Dimensions.get("window").width * 0.29,
		flexDirection:"row",
		justifyContent:"space-between",
		alignItems:"center",
		marginRight:20
	},
	year:{
		// paddingRight:3
	}
})
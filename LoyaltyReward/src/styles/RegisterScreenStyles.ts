import {StyleSheet,Dimensions} from "react-native";
import {notificationStyles} from "./LoginScreenStyles";

export const styles = StyleSheet.create({
	main:{
		flexDirection:"column",
		flex:1,
	},
	register:{
		height:Dimensions.get("window").height * 0.73,
		// marginTop:200,
		justifyContent:"flex-end",
		marginLeft:30,
		// backgroundColor:"#ff0000",
		marginRight:30
	},
	title:{
		fontSize:30,
		fontWeight:"600",
		marginBottom:20
	},
	textInput:{
		marginTop:3,
		marginBottom:10,
		fontSize:20,
		borderWidth:1,
		borderColor:"#adadad",
		width:300,
		height:40,
		borderRadius:5
	},
	notificationContainer:{
		marginTop:10,
		height:40,
		// backgroundColor:"#ff0000"
	},
	notification:{
		marginLeft:30,
		marginRight:30,
		...notificationStyles.successColor,
		...notificationStyles.commonNotification
	},
	dangerNotification:{
		marginLeft:30,
		marginRight:30,
		...notificationStyles.dangerColor,
		...notificationStyles.commonNotification,
		marginBottom:4
	},
	notificationText:{
		fontSize:18,
		color:"#adadad",
		fontWeight:"600",
		marginBottom:4
	},
	dangerNotificationText:{
		fontSize:18,
		color:"#ffffff",
		fontWeight:"600"
	},
	registerContainer:{
		justifyContent:"space-between",
		alignItems:"flex-end",
		flexDirection:"row",
		marginTop:55,
		marginLeft:30,
		marginRight:30,
		marginBottom:30,
		// flex:1,
		// backgroundColor:"#ff0000"
	},
	loginButton:{
		fontSize:14,
		fontWeight:"600",
		color:"#ffffff"
	},
	registerButton:{
		...notificationStyles.button
	},
	button:{
		...notificationStyles.buttonText
	}
})
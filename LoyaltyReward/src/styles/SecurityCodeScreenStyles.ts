import {StyleSheet,Dimensions} from "react-native";
import {notificationStyles} from "./LoginScreenStyles";

export const styles = StyleSheet.create({
	main:{
		flexDirection:"column",
		flex:1,
	},
	register:{
		height:Dimensions.get("window").height * 0.45,
		// marginTop:200,
		justifyContent:"flex-end",
		marginLeft:30,
		// backgroundColor:"#ff0000",
		marginRight:30
	},
	title:{
		fontSize:30,
		fontWeight:"600",
		marginBottom:20
	},
	textInput:{
		marginTop:3,
		marginBottom:10,
		fontSize:20,
		borderWidth:1,
		borderColor:"#adadad",
		width:300,
		height:40,
		borderRadius:5
	},
	blankContainer:{
		height:42
	},
	notificationContainer:{
		marginTop:10,
		height:40,
		// backgroundColor:"#ff0000"
	},
	notification:{
		marginLeft:30,
		marginRight:30,
		...notificationStyles.successColor,
		...notificationStyles.commonNotification
	},
	dangerNotification:{
		marginLeft:30,
		marginRight:30,
		...notificationStyles.dangerColor,
		...notificationStyles.commonNotification,
		marginBottom:4,

	},
	notificationText:{
		fontSize:18,
		color:"#adadad",
		fontWeight:"600",
		marginBottom:4
	},
	dangerNotificationText:{
		fontSize:18,
		color:"#ffffff",
		fontWeight:"600"
	},
	registerContainer:{
		justifyContent:"space-between",
		alignItems:"flex-end",
		flexDirection:"row",
		marginTop:125,
		marginLeft:30,
		marginRight:30,
		marginBottom:10,
		flexWrap:"wrap"
		// flex:1,
		// backgroundColor:"#ff0000"
	},
	resendContainer:{
		justifyContent:"space-between",
		alignItems:"flex-end",
		flexDirection:"row",
		marginTop:5,
		marginLeft:30,
		marginRight:30,
		marginBottom:30,
		flexWrap:"wrap"
		// flex:1,
		// backgroundColor:"#ff0000"
	},
	registerResendContainer:{
		justifyContent:"space-between",
		alignItems:"flex-end",
		flexDirection:"row",
		marginTop:15,
		marginHorizontal:20,
		marginBottom:30,
		flexWrap:"wrap"
		// flex:1,
		// backgroundColor:"#ff0000"
	},
	resendText:{
		color:"#aaaaaa"
	},
	loginButton:{
		fontSize:14,
		fontWeight:"600",
		color:"#ffffff"
	},
	registerButton:{
		...notificationStyles.button
	},
	button:{
		...notificationStyles.buttonText
	},
	instructionContainer:{

	},
	buttonContainer:{

	},
	securityCodeContainer:{
		flexDirection:"row",
		justifyContent:"center",
	},
	securityCodeBox:{
		width:50,
		height:60,
		marginRight:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:5,
		fontSize:40,
		justifyContent:"center",
		alignItems:"center",
		padding:10,
		color:"#999999",
		// justifyContent:"center",
		// alignItems:"center",
	},
	securityCodeBoxMargin:{
		width:50,
		height:60,
		marginRight:5,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:5,
		fontSize:40,
		justifyContent:"center",
		alignItems:"center",
		padding:10,
		color:"#999999",
		// justifyContent:"center",
		// alignItems:"center",
	},
	// securityCodeText:{

	// },
	securityCodeTextMargin:{
		// width:50,
		// height:60,
		// marginRight:5,
		// borderWidth:1,
		// borderColor:"#adadad",
		// borderRadius:5,
		fontSize:40,
		// padding:10,
		color:"#999999"
	},
	securityCodeText:{
		fontSize:40,
		// padding:10,
		color:"#999999"
	},
	submitButton:{
		...notificationStyles.button
	},
	confirmSubmitButton:{
		flexDirection:"row",
		borderWidth:2,
		borderColor:"#117700",
		paddingLeft:20,
		paddingRight:20,
		paddingTop:10,
		paddingBottom:10,
		borderRadius:5,
	},
	buttonText:{
		fontSize:20,
		fontWeight:"600",
		color:"#117700"
	}
})
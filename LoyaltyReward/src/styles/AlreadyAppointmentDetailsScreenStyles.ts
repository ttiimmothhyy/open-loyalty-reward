import {Dimensions,StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	pageContainer:{
		height:Dimensions.get("window").height
	},
	imageContainer:{
		margin:10,
		borderWidth:1,
		borderColor:"#adadad",
	},
	image:{
		height:Dimensions.get("window").height * 0.35,
		width:Dimensions.get("window").width * 0.94,
	},
	title:{
		marginHorizontal:20,
		fontSize:22,
		fontWeight:"800"
	},
	timeContainer:{
		flexDirection:"row",
		alignItems:"center",
		justifyContent:"space-between",
		paddingHorizontal:20,
		paddingVertical:5,
		paddingTop:15
	},
	placeContainer:{
		flexDirection:"row",
		alignItems:"center",
		justifyContent:"space-between",
		paddingHorizontal:20,
		paddingVertical:5
	},
	timeText:{
		fontSize:16
	},
	backButton:{
		alignItems:"center",
		justifyContent:"center",
		backgroundColor:"#eedd8d",
		paddingVertical:15,
		marginHorizontal:20,
		borderRadius:5,
		marginTop:40,
	},
	backText:{
		color:"#ffffff",
		fontWeight:"800",
		fontSize:18
	}
})
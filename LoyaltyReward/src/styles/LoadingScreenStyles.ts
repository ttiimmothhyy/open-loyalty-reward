import {StyleSheet,Dimensions} from "react-native";

export const styles = StyleSheet.create({
	main:{
		flex:1,
		flexDirection:"column",
		justifyContent:"center"
	},
	image:{
		height:50,
		width:50,
		marginLeft:30,
		marginTop:140
	},
	title:{
		fontSize:40,
		fontWeight:"500",
		marginLeft:30
	},
	subTitle:{
		marginTop:5,
		marginLeft:30,
		fontSize:20
	}
})

export const backgroundStyles = StyleSheet.create({
	image:{
		flex:1,
		resizeMode:"cover",
		justifyContent:"center",
		flexDirection:"column",
		width:Dimensions.get("window").width,
		height:Dimensions.get("window").height,
		// position:"relative"
		// position:"absolute",
		// top:0,
		// left:0,
		// right:0,
		// bottom:0
	},
})

export const flexStyles = StyleSheet.create({
	flex:{
		flex:1,
	},
})

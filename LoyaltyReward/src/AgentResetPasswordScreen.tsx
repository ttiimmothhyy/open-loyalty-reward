import {useNavigation} from "@react-navigation/native";
import React,{useState} from "react";
import {useEffect} from "react";
import {SafeAreaView,Keyboard,KeyboardAvoidingView,Platform,TouchableWithoutFeedback,View,Text,TextInput,TouchableOpacity,
ImageBackground} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {resetNull,resetAgentPasswordByEmail} from "./redux/agentUser/thunks";
import {IRootState} from "../store";
import {backgroundStyles} from "./styles/LoadingScreenStyles";
import {styles} from "./styles/AgentResetPasswordScreenStyles";
import {AgentRoleCheckBox} from "./components/CheckBox";

function AgentResetPasswordScreen(){
	const [email,setEmail] = useState("");
	const [wrongEmail,setWrongEmail] = useState(false)
	const dispatch = useDispatch();
	const navigation = useNavigation();
	const isResetAgent = useSelector((state:IRootState)=>state.agentUser.isReset);

	useEffect(() => {
		if(isResetAgent){
			setTimeout(() => {
				navigation.navigate("RoleLogin")
			},1000)
		}
	},[isResetAgent])

	return(
		<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
			<ImageBackground source={require("../images/agentBackground.png")} style={backgroundStyles.image}>
				<SafeAreaView style={styles.main}>
					<AgentRoleCheckBox/>
					<View style={styles.emailInput}>
						<View><Text style={styles.title}>重設密碼</Text></View>
						<TextInput style={styles.textInput} placeholder="電郵" value={email} onChangeText={(event)=>{
							setEmail(event)
							setWrongEmail(false)
							dispatch(resetNull())
						}}></TextInput>
						<View style={styles.buttonContainer}>
							<TouchableOpacity onPress={()=>{
								navigation.navigate("RoleLogin")
								setWrongEmail(false)
								dispatch(resetNull())
							}}>
								<View style={styles.sendButton}>
									<Text style={styles.button}>返回登入頁面</Text>
								</View>
							</TouchableOpacity>
							<TouchableOpacity onPress={()=>{
								{!email.includes("@") && setWrongEmail(true)}
								{email.includes("@") && dispatch(resetAgentPasswordByEmail(email))}
							}}>
								<View style={styles.sendButton}>
									<Text style={styles.button}>傳送</Text>
								</View>
							</TouchableOpacity>
						</View>
						<View style={styles.notificationContainer}>
							{isResetAgent && (
								<View style={styles.notification}>
									<Text style={styles.notificationText}>已傳送</Text>
								</View>
							)}
							{wrongEmail && (
								<View style={styles.dangerNotification}>
									<Text style={styles.dangerNotificationText}>這電郵不正確</Text>
								</View>
							)}
							{isResetAgent === false && (
								<View style={styles.dangerNotification}>
									<Text style={styles.dangerNotificationText}>此電郵沒有登記資料</Text>
								</View>
							)}
						</View>
					</View>
				</SafeAreaView>
			</ImageBackground>
		</TouchableWithoutFeedback>
	)
}

export default AgentResetPasswordScreen;
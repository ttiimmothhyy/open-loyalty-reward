import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {useNavigation} from "@react-navigation/native";
import React,{useEffect,useState} from "react";
import {useColorScheme,View,Text,TouchableOpacity,ScrollView,Image} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {styles} from "./styles/GiftCartScreenStyles";
import {useDispatch,useSelector} from "react-redux";
import {addToCart,deleteFromCart,getInStockItem,getOutOfStockItem,minusFromCart} from "./redux/redeem/thunks";
import envfile from "../envfile";
import {IRootState} from "../store";

// define inStockItems
export const InStockItemInCart:React.FC<{id:string,name:string,imageSource:string,redeem_points:string,
type:string,amount_in_cart:string}> = ({id,name,imageSource,redeem_points,type,amount_in_cart}) => {
	// console.log(name + " (" + parseInt(amount_in_cart) + ") a")
	const [value,setValue] = useState(parseInt(amount_in_cart));
	const dispatch = useDispatch();
	const rewardId = parseInt(id);
	// console.log(name + " (" + value + ") b")

	return(
		<View style={styles.giftOtherContainer}>
			<View style={styles.description}>
				<View>
					<Text style={styles.name}>{name}</Text>
					<Text style={styles.giftType}>{type}</Text>
					<View style={styles.pointRowContainer}>
						<Text>{parseInt(redeem_points) * parseInt(amount_in_cart)} pt</Text>
						<Text style={styles.haveText}>有貨</Text>
					</View>
					<View style={styles.numberRowContainer}>
						<View style={styles.numberContainer}>
							<TouchableOpacity style={styles.leftMathButton} onPress={() => {
								setValue(value > 0 ? value - 1 : value)
								{value > 0 ? dispatch(minusFromCart(rewardId)):""};
							}}><Text style={styles.numberText}>-</Text></TouchableOpacity>
							<View style={styles.mathButton}>
								<Text style={styles.numberText}>{value}</Text>
							</View>
							<TouchableOpacity style={styles.rightMathButton} onPress={() => {
								setValue(value + 1)
								{dispatch(addToCart(rewardId))};
							}}><Text style={styles.numberText}>+</Text></TouchableOpacity>
						</View>
						<TouchableOpacity style={styles.trash} onPress={() => {
							{dispatch(deleteFromCart(rewardId))};
						}}>
							<FontAwesomeIcon icon="trash-restore" size={12} style={styles.numberText}/>
						</TouchableOpacity>
					</View>
				</View>
			</View>
			<View style={styles.imageContainer}>
			<Image style={styles.image} source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/rewards/${imageSource}`}}/>
			</View>
		</View>
	)
}

// define outOfStockItems
export const OutOfStockItemInCart:React.FC<{id:string,name:string,imageSource:string,redeem_points:string,
type:string,amount_in_cart:string}> = ({id,name,imageSource,redeem_points,type,amount_in_cart}) => {
// redeem_points,type,remain_amount,amount_in_cart}) => {
	// console.log(name + " (" + parseInt(amount_in_cart) + ") c")
	const [outValue,setOutValue] = useState(parseInt(amount_in_cart));
	const dispatch = useDispatch();
	const rewardId = parseInt(id);
	// console.log(name + " (" + outValue + ") d")

	return(
		<View style={styles.giftOtherContainer}>
			<View style={styles.description}>
				<View>
					<Text style={styles.name}>{name}</Text>
					<Text style={styles.giftType}>{type}</Text>
					<View style={styles.pointRowContainer}>
						<Text>{parseInt(redeem_points) * parseInt(amount_in_cart)} pt</Text>
						<Text style={styles.haveText}>缺貨中</Text>
					</View>
					<View style={styles.numberRowContainer}>
						<View style={styles.numberContainer}>
							<TouchableOpacity style={styles.leftMathButton} onPress={() => {
								setOutValue(outValue > 0 ? outValue - 1 : outValue)
								{outValue > 0 ? dispatch(minusFromCart(rewardId)):""};
							}}><Text style={styles.numberText}>-</Text></TouchableOpacity>
							<View style={styles.mathButton}>
								<Text style={styles.numberText}>{outValue}</Text>
							</View>
							<TouchableOpacity style={styles.rightMathButton} onPress={() => {
								setOutValue(outValue + 1)
								{dispatch(addToCart(rewardId))};
							}}><Text style={styles.numberText}>+</Text></TouchableOpacity>
						</View>
						<TouchableOpacity style={styles.trash} onPress={() => {
							{dispatch(deleteFromCart(rewardId))};
						}}>
							<FontAwesomeIcon icon="trash-restore" size={12} style={styles.numberText}/>
						</TouchableOpacity>
					</View>
				</View>
			</View>
			<View style={styles.imageContainer}>
			<Image style={styles.image} source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/rewards/${imageSource}`}}/>
			</View>
		</View>
	)
}

function GiftCartScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const inStockItemList = useSelector((state:IRootState)=>state.redeemPage.inStockItemDetail);
	// console.log(inStockItemList)
	const outOfStockItemList = useSelector((state:IRootState)=>state.redeemPage.outOfStockItemDetail);
	// console.log(outOfStockItemList)
	const addSuccess = useSelector((state:IRootState)=>state.redeemPage.addToCartResult);
	const minusSuccess = useSelector((state:IRootState)=>state.redeemPage.minusFromCartResult);
	const deleteSuccess = useSelector((state:IRootState)=>state.redeemPage.deleteFromCartResult);
	// console.log(deleteSuccess + " delete")
	// console.log(addSuccess + " add")
	// console.log(minusSuccess + " minus")

	useEffect(() => {
		dispatch(getInStockItem());
		dispatch(getOutOfStockItem());
	},[dispatch])

	useEffect(() => {
		if(addSuccess || minusSuccess || deleteSuccess){
			dispatch(getInStockItem());
			dispatch(getOutOfStockItem());
		}
	},[dispatch,addSuccess,minusSuccess,deleteSuccess])

	return(
		<View style={[styles.flexContainer,{backgroundColor: isDarkMode ? Colors.black : Colors.white}]}>
			<View style={styles.titleContainer}>
				<View>
					<Text>
						{inStockItemList.map(inStockItem => parseInt(inStockItem.amount_in_cart))
						.reduce((initValue, inStockItem) => initValue + inStockItem, 0)
						+ outOfStockItemList.map(outOfStockItem => parseInt(outOfStockItem.amount_in_cart))
						.reduce((initValue, eachAmount) => initValue + eachAmount, 0)}項禮物
					</Text>
				</View>
			</View>
			<View style={styles.scrollView}>
				<ScrollView>
					<View style={styles.haveStock}>
						<Text style={styles.haveStockText}>有貨</Text>
					</View>
					{inStockItemList.length > 0 && inStockItemList.map((inStockItem,index)=>
						{
							// console.log(inStockItem.name + " (" + inStockItem.amount_in_cart + ") c")
							return (<InStockItemInCart key={inStockItem.latest_cart_id} id={inStockItem.id}
							imageSource={inStockItem.image} name={inStockItem.name}
							redeem_points={inStockItem.redeem_points} type={inStockItem.type}
							amount_in_cart={inStockItem.amount_in_cart}/>
						)}
					)}
					<View style={styles.noStock}>
						<Text style={styles.noStockText}>缺貨中</Text>
					</View>
					{outOfStockItemList.length > 0 && outOfStockItemList.map((outOfStockItem,index)=>(
						<OutOfStockItemInCart key={outOfStockItem.latest_cart_id} id={outOfStockItem.id} imageSource={outOfStockItem.image}
						name={outOfStockItem.name} redeem_points={outOfStockItem.redeem_points}
						type={outOfStockItem.type} amount_in_cart={outOfStockItem.amount_in_cart}/>
					))}
				</ScrollView>
				<TouchableOpacity style={styles.confirmButton} onPress={() => {
					navigation.navigate("ConfirmRedemptionScreen")
				}}>
					<Text style={styles.confirmText}>確認</Text>
				</TouchableOpacity>
				<TouchableOpacity style={styles.backButton} onPress={() => {
					navigation.navigate("GiftScreen")
				}}>
					<Text style={styles.backText}>返回</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}

export default GiftCartScreen;
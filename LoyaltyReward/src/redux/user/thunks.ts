import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {IUserActions,registerFailure,registerNullSuccess,registerSuccess,resendSecurityCodeFailure,resendSecurityCodeSuccess,resetNullSuccess,resetPasswordFailure,
		resetPasswordSuccess,verifySecurityCodeFailure,verifySecurityCodeSuccess} from "./actions";

export function register(username:string,password:string,display_name:string,email:string,mobile:number,referral_mobile:number){
	return async(dispatch:Dispatch<IUserActions>) => {
		// console.log(username)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/register`,{
			method:"post",
			headers:{
				"Content-Type":"application/json",
			},
			body:JSON.stringify({username,password,display_name,email,mobile,referral_mobile})
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(registerSuccess(result.user_id))
		}else{
			dispatch(registerFailure(result.message));
		}
	}
}

export function userRegister(username:string,password:string,name:string,email:string,phone:number,suggestedPhone:number){
	return async(dispatch:Dispatch<IUserActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/userRegister`,{
			method:"post",
			headers:{
				"Content-Type":"application/json",
			},
			body:JSON.stringify({username,password,name,email,phone,suggestedPhone,role:"customer"})
		})
		const result = await res.json();
		if(result.success){
			dispatch(registerSuccess(result.user_id))
		}else{
			dispatch(registerFailure(result.message));
		}
	}
}

export function registerNull(){
	return async (dispatch:Dispatch<IUserActions>)=>{
        dispatch(registerNullSuccess());
    }
}

export function resetPasswordByEmail(email:string){
	return async(dispatch:Dispatch<IUserActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/resetPassword`,{
			method:"post",
			headers:{
				"Content-Type": "application/json"
			},
			body:JSON.stringify({email})
		});
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(resetPasswordSuccess());
		}else{
			dispatch(resetPasswordFailure(result.message));
		}
	}
}

export function resetNull(){
	return async (dispatch:Dispatch<IUserActions>)=>{
        dispatch(resetNullSuccess());
    }
}

//check whether the Security code is correct
export function verifySecurityCode(userId:string,securityCode:string){
	return async(dispatch:Dispatch<IUserActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/registerOTP/${userId}/${securityCode}`,{
			headers:{
				"Content-Type": "application/json"
			}
		});
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(verifySecurityCodeSuccess(result));
		}else{
			dispatch(verifySecurityCodeFailure(result.message));
		}
	}
}

//request to re-send security code
export function resendSecurityCode(userId:string){
	return async(dispatch:Dispatch<IUserActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/register/OTPResend/${userId}/`,{
			headers:{
				"Content-Type": "application/json"
			}
		});
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(resendSecurityCodeSuccess(result));
		}else{
			dispatch(resendSecurityCodeFailure(result.message));
		}
	}
}
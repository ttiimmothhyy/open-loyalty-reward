import {ExpiredPointsHistoryRow, PointsEarnedHistoryRow,PointsRedeemedHistoryRow} from "../../../models";

export function getPointsEarnedHistorySuccess(pointsEarnedHistoryRow:PointsEarnedHistoryRow[]){
    return{
        type:"@@point/Get_points_earned_history" as const,
        pointsEarnedHistoryRow
    }
}

export function getPointsRedeemedHistorySuccess(pointsRedeemedHistoryRow:PointsRedeemedHistoryRow[]){
    return{
        type:"@@point/Get_points_redeemed_history" as const,
        pointsRedeemedHistoryRow
    }
}

export function getExpiredPointsHistorySuccess(expiredPointHistory:ExpiredPointsHistoryRow[]){
	return{
		type:"@@point/Get_expired_points_history" as const,
		expiredPointHistory
	}
}

type Failed = "Get_points_earned_history_failed"|"Get_points_redeemed_history_failed"|"Get_expired_points_history_failed";
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IPointsActions = ReturnType<typeof getPointsEarnedHistorySuccess|typeof getPointsRedeemedHistorySuccess|typeof getExpiredPointsHistorySuccess|
typeof failed>
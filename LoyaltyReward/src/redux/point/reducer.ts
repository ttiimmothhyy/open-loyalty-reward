import {ExpiredPointsHistoryRow, PointsEarnedHistoryRow,PointsRedeemedHistoryRow} from "../../../models";
import {IPointsActions} from "./actions";

export interface IPointState{
    pointsEarnedHistoryRow:PointsEarnedHistoryRow[];
    pointsRedeemedHistoryRow:PointsRedeemedHistoryRow[];
	expiredPointHistory:ExpiredPointsHistoryRow[];
}

const initialState = {
    pointsEarnedHistoryRow:[],
    pointsRedeemedHistoryRow:[],
	expiredPointHistory:[]
}

const pointReducer = (state:IPointState = initialState,action:IPointsActions):IPointState => {
    switch(action.type){
        case "@@point/Get_points_earned_history":
            return{
                ...state,
                pointsEarnedHistoryRow:action.pointsEarnedHistoryRow
            }
        case "@@point/Get_points_redeemed_history":
            return{
                ...state,
                pointsRedeemedHistoryRow:action.pointsRedeemedHistoryRow
            }
		case "@@point/Get_expired_points_history":{
			return{
				...state,
				expiredPointHistory:action.expiredPointHistory
			}
		}
        default:
            return state;
    }
}

export default pointReducer;
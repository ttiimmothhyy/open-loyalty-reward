import {ISocialAuthActions} from "./actions";

export interface IReactNativeAuthToken{
	accessToken:string;
	accessTokenExpirationDate:string;
	refreshToken:string;
}

export interface ISocialAgentAuthState{
	hasLoggedInOnce:boolean;
	provider:string;
	accessToken:string;
	accessTokenExpirationDate:string;
	refreshToken:string;
}

const initialState = {
	hasLoggedInOnce:false,
	provider:"",
	accessToken:"",
	accessTokenExpirationDate:"",
	refreshToken:""
}

const socialAgentAuthReducer = (state:ISocialAgentAuthState = initialState,action:ISocialAuthActions):ISocialAgentAuthState => {
	switch(action.type){
		case "@@socialAgentAuth/Login_google":
			return{
				...state,
				hasLoggedInOnce:true,
				provider:"google",
				...action.token
			}
		default:
			return state
	}
}

export default socialAgentAuthReducer;

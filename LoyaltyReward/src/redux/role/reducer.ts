import {IRoleActions} from "./actions";

export interface IRoleState{
	role:string;
	slider:number
}

const initialState:IRoleState = {
	role:"customer",
	slider:0
}

const roleReducer = (state:IRoleState = initialState,action:IRoleActions):IRoleState => {
	switch(action.type){
		case "@@role/Change_role":
			return{
				...state,
				role:action.role
			}
		default:
			return state
	}
}

export default roleReducer;
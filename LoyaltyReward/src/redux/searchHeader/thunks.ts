import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {ISearchHeaderActions,searchKeywordSuccess,searchKeywordSuccessAgent,failed,updateSearchWord, searchKeywordFail, searchKeywordFailAgent} from "./actions";

export function searchKeyword(word:string,isAuthenticated:boolean|null,isAuthenticatedAgent:boolean|null){
	return async(dispatch:Dispatch<ISearchHeaderActions>) => {
		// console.log(isAuthenticated + " 1")
		// console.log(isAuthenticatedAgent + " 2")
		// console.log(word)
		const token = await AsyncStorage.getItem("token");
		if(isAuthenticated){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/search/${word}`,{
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			const result = await res.json()
			// console.log(1)
			if(result.treatments.length > 0){
				// console.log(result)
				dispatch(searchKeywordSuccess(result))
				dispatch(searchKeywordFailAgent())
				// dispatch(updateSearchWord(""))
			}else{
				dispatch(searchKeywordFail())
				dispatch(failed("Search_keyword_failed",result.message))
			}
		}else if(isAuthenticatedAgent){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/search/${word}`,{
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			const result = await res.json()
			// console.log(result)
			if(result){
				dispatch(searchKeywordSuccessAgent(result))
				dispatch(searchKeywordFail())
				// dispatch(updateSearchWord(""))
				// console.log(result)
			}else{
				dispatch(searchKeywordFailAgent())
				dispatch(dispatch(failed("Search_keyword_failed",result.message)))
			}
		}
	}
}
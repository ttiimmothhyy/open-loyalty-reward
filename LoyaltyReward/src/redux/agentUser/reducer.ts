import {IAgentUserActions} from "./actions";

export interface IAgentUserState{
	isReset:boolean|null;
	message:string;
}

export const initialState = {
	isReset:null,
	message:""
}

const agentUserReducer = (state:IAgentUserState = initialState,action:IAgentUserActions):IAgentUserState => {
	switch(action.type){
		case "@@user/Reset_password_success":
			return{
				...state,
				isReset:true
			}
		case "@@user/Reset_password_failure":
			return{
				...state,
				isReset:false,
				message:action.message
			}
		case "@@user/Reset_null":
			return{
				...state,
				isReset:null
			}
		default:
			return state
	}
}

export default agentUserReducer;
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {IAgentUserActions,resetNullSuccess,resetPasswordFailure,
resetPasswordSuccess} from "./actions";

export function resetAgentPasswordByEmail(email:string){
	return async(dispatch:Dispatch<IAgentUserActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/resetPassword`,{
			method:"post",
			headers:{
				"Content-Type": "application/json"
			},
			body:JSON.stringify({email})
		});
		const result = await res.json();
		if(result.success){
			dispatch(resetPasswordSuccess());
		}else{
			dispatch(resetPasswordFailure(result.message));
		}
	}
}

export function resetNull(){
	return async (dispatch:Dispatch<IAgentUserActions>)=>{
        dispatch(resetNullSuccess());
    }
}

import {ISocialAuthActions} from "./actions";

export interface IReactNativeAuthToken{
	accessToken:string;
	accessTokenExpirationDate:string;
	refreshToken:string;
}

export interface ISocialAuthState{
	hasLoggedInOnce:boolean;
	provider:string;
	accessToken:string;
	accessTokenExpirationDate:string;
	refreshToken:string;
}

const initialState = {
	hasLoggedInOnce:false,
	provider:"",
	accessToken:"",
	accessTokenExpirationDate:"",
	refreshToken:""
}

const socialAuthReducer = (state:ISocialAuthState = initialState,action:ISocialAuthActions):ISocialAuthState => {
	switch(action.type){
		case "@@socialAuth/Login_google":
			return{
				...state,
				hasLoggedInOnce:true,
				provider:"google",
				...action.token
			}
		default:
			return state
	}
}

export default socialAuthReducer;

import AsyncStorage from "@react-native-community/async-storage";
import {FBAccessToken} from "react-native-fbsdk-next/types/FBAccessToken";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {IAuthActions,loginFailure,loginSuccess} from "../auth/actions";
import {ISocialAuthActions} from "./actions";

export function handleGoogleAuthorize(accessToken:string){
	return async(dispatch:Dispatch<IAuthActions|ISocialAuthActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/googleLogin/customers`,{
			method:"post",
			headers:{
				"Content-Type":"application/json"
			},
			body:JSON.stringify({accessToken,role:"customer"})
		});
		const result = await res.json();
		// console.log(result);
		if(result.success){
			dispatch(loginSuccess(result.id,result.data));
			await AsyncStorage.setItem("token",result.data)
		}else{
			dispatch(loginFailure(result.message));
		}
	}
}

export function handleFacebookAuthorize(accessToken:string|undefined,email:string|undefined){
	return async(dispatch:Dispatch<IAuthActions|ISocialAuthActions>) => {
		// console.log(accessToken)
		// console.log(email)
		if(accessToken && email){
			// console.log(accessToken)
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/facebookLogin/customers`,{
				method:"post",
				headers:{
					"Content-Type":"application/json"
				},
				body:JSON.stringify({accessToken,email})
			});
			const result = await res.json();
			// console.log(result);
			if(result.success){
				dispatch(loginSuccess(result.id,result.data));
				await AsyncStorage.setItem("token",result.data)
			}else{
				dispatch(loginFailure(result.message));
			}
		}
	}
}

export function handleAppleAuthorize(clientId:string,identityToken:string|null,authorizationCode:string|null){
	return async(dispatch:Dispatch<IAuthActions|ISocialAuthActions>) => {
		// console.log(accessToken)
		// console.log(email)
		// console.log(accessToken)
		if(identityToken && authorizationCode){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/appleLogin/customers`,{
				method:"post",
				headers:{
					"Content-Type":"application/json"
				},
				body:JSON.stringify({clientId,identityToken,authorizationCode})
			});
			const result = await res.json();
			// console.log(result);
			if(result.success){
				dispatch(loginSuccess(result.id,result.data));
				await AsyncStorage.setItem("token",result.data)
			}else{
				dispatch(loginFailure(result.message));
			}
		}
	}
}
import {Advertisement,ContactCard,PerformanceCount} from "../../../models";
import {IAgentHomeActions} from "./actions";

export interface IAgentHomeState{
	advertisement:Advertisement[];
	contact:ContactCard[];
	performance:PerformanceCount
}

const initialState = {
	advertisement:[],
	contact:[],
	performance:{
		appointment_count:0,
		referral_count:0,
	}
}

const agentHomeReducer = (state:IAgentHomeState = initialState,action:IAgentHomeActions):IAgentHomeState => {
	switch(action.type){
		case "@@agentHome/Get_advertisement":
			return{
				...state,
				advertisement:action.advertisement
			}
		case "@@agentHome/Get_contact_card":
			return{
				...state,
				contact:action.contactCard
			}
		case "@@agentHome/Get_my_performance":
			return{
				...state,
				performance:action.count
			}
		default:
			return state
	}
}

export default agentHomeReducer;
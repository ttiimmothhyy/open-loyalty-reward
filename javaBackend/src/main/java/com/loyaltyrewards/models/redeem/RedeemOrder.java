package com.loyaltyrewards.models.redeem;

import com.loyaltyrewards.models.user.Customer;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity(name="redeem_order")
public class RedeemOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    @Column(nullable = false)
    private Date redeem_date;

    @Column(nullable = false)
    private Date redeem_expires_date;

    @Column(nullable = false)
    private Integer total_redeem_points;

    private String remark;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    private Long order_ref;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_pickup;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_deleted;

    @ManyToOne
    @JoinColumn(name="pickup_location_id")
    private PickupLocation pickup_location;



}

package com.loyaltyrewards.models.user;

import com.loyaltyrewards.models.agent.Agent;
import com.loyaltyrewards.models.treatment.Score;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity(name="users")
public class User implements UserDetails {
    // -------------- create user table --------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true,nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column
    private String token;

    @Column(unique=true,nullable = false)
    private String email;

    @Column(unique=true)
    private Long mobile;

    @ManyToOne
    @JoinColumn(name="role_id")
    private Role role;

    @ManyToOne
    @JoinColumn(name="register_status_id")
    private RegisterStatus status;

    @Column(name = "uuid")
    private UUID uuid;

    private String OTP;

    private String display_name;

    @ManyToOne
    @JoinColumn(name="gender_id")
    private Gender gender;

    private Date date_of_birth;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    @OneToOne(mappedBy = "users")
    private Customer customer;

    @OneToOne(mappedBy = "users")
    private Agent agent;

    private Integer referral_mobile;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_deleted;

    private Integer Referral_agent_id;

//    public User(String username, String password, String email,
//                Integer mobile, Role role, RegisterStatus status,
//                UUID uuid, String display_name, Gender gender, Date date_of_birth) {
//        this.username = username;
//        this.password = password;
//        this.email = email;
//        this.mobile = mobile;
//        this.role = role;
//        this.status = status;
//        this.uuid = uuid;
//        this.display_name = display_name;
//        this.gender = gender;
//        this.date_of_birth = date_of_birth;
//    }

    // -------------- setter --------------

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    // -------------- get service --------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public RegisterStatus getStatus() {
        return status;
    }

    public void setStatus(RegisterStatus status) {
        this.status = status;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
    public Integer getReferral_agent_id() {
        return Referral_agent_id;
    }

    public Integer getReferral_mobile() {
        return referral_mobile;
    }

    public void setReferral_mobile(Integer referral_mobile) {
        this.referral_mobile = referral_mobile;
    }

    public void setReferral_agent_id(Integer referral_agent_id) {
        Referral_agent_id = referral_agent_id;
    }

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }
}

package com.loyaltyrewards.models.user;

import com.loyaltyrewards.models.agent.AgentContact;
import com.loyaltyrewards.models.agent.ReferralCommission;
import com.loyaltyrewards.models.purchase.PurchasedItem;
import com.loyaltyrewards.models.treatment.Appointment;
import com.loyaltyrewards.models.enquiry.Enquiry;
import com.loyaltyrewards.models.point.EarnedPoint;
import com.loyaltyrewards.models.point.ExpiredPoint;
import com.loyaltyrewards.models.redeem.CartItem;
import com.loyaltyrewards.models.redeem.RedeemOrder;
import com.loyaltyrewards.models.treatment.Score;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity(name="customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToOne
    @JoinColumn(name="user_id")
    private User users;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    @Column(columnDefinition = "boolean default true",nullable = false)
    private Boolean notification;

    @OneToMany(mappedBy = "customer")
    private List<Appointment> Appointment;

    @OneToMany(mappedBy = "customer")
    private List<CartItem> cartItem;

    @OneToMany(mappedBy = "customer")
    private List<PurchasedItem> purchasedItem;

    @OneToMany(mappedBy = "customer")
    private List<RedeemOrder> redeemOrder;

    @OneToMany(mappedBy = "customer")
    private List<EarnedPoint> earnedPoint;

    @OneToMany(mappedBy = "customer")
    private List<ExpiredPoint> expiredPoint;

    @OneToMany(mappedBy = "customer")
    private List<Enquiry> enquiry;

    @OneToMany(mappedBy = "customer")
    private List<AgentContact> agentContact;

    @OneToOne(mappedBy = "customer")
    private PythonGroup pythonGroup;


    @OneToMany(mappedBy = "customer")
    private List<ReferralCommission> referralCommissions;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUsers() {
        return users;
    }

    public void setUsers(User users) {
        this.users = users;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getNotification() {
        return notification;
    }

    public void setNotification(Boolean notification) {
        this.notification = notification;
    }

    public List<com.loyaltyrewards.models.treatment.Appointment> getAppointment() {
        return Appointment;
    }

    public void setAppointment(List<com.loyaltyrewards.models.treatment.Appointment> appointment) {
        Appointment = appointment;
    }

    public List<CartItem> getCartItem() {
        return cartItem;
    }

    public void setCartItem(List<CartItem> cartItem) {
        this.cartItem = cartItem;
    }

    public List<RedeemOrder> getRedeemOrder() {
        return redeemOrder;
    }

    public void setRedeemOrder(List<RedeemOrder> redeemOrder) {
        this.redeemOrder = redeemOrder;
    }

    public List<EarnedPoint> getEarnedPoint() {
        return earnedPoint;
    }

    public void setEarnedPoint(List<EarnedPoint> earnedPoint) {
        this.earnedPoint = earnedPoint;
    }

    public List<ExpiredPoint> getExpiredPoint() {
        return expiredPoint;
    }

    public void setExpiredPoint(List<ExpiredPoint> expiredPoint) {
        this.expiredPoint = expiredPoint;
    }

    public List<Enquiry> getEnquiry() {
        return enquiry;
    }

    public void setEnquiry(List<Enquiry> enquiry) {
        this.enquiry = enquiry;
    }

    public List<AgentContact> getAgentContact() {
        return agentContact;
    }

    public void setAgentContact(List<AgentContact> agentContact) {
        this.agentContact = agentContact;
    }

    public List<ReferralCommission> getReferralCommissions() {
        return referralCommissions;
    }

    public void setReferralCommissions(List<ReferralCommission> referralCommissions) {
        this.referralCommissions = referralCommissions;
    }

}

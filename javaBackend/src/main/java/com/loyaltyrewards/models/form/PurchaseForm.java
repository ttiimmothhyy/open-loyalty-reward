package com.loyaltyrewards.models.form;

public class PurchaseForm {
    private Long id;
    private Integer purchased_amount;
    private Long treatment_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Integer getPurchased_amount() {
        return purchased_amount;
    }

    public void setPurchased_amount(Integer purchased_amount) {
        this.purchased_amount = purchased_amount;
    }


    public Long getTreatment_id() {
        return treatment_id;
    }

    public void setTreatment_id(Long treatment_id) {
        this.treatment_id = treatment_id;
    }
}

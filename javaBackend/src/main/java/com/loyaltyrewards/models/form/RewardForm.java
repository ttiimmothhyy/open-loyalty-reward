package com.loyaltyrewards.models.form;

import com.loyaltyrewards.models.redeem.PickupLocation;
import com.loyaltyrewards.models.redeem.RewardType;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public class RewardForm {

    private String name;
    private String detail;
    private String image;
    private Integer redeem_points;
    private Integer remain_amount;
    private Boolean is_hidden;
    private Integer reward_type;
    private List<Integer> pickup_location;
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getRedeem_points() {
        return redeem_points;
    }

    public void setRedeem_points(Integer redeem_points) {
        this.redeem_points = redeem_points;
    }

    public Integer getRemain_amount() {
        return remain_amount;
    }

    public void setRemain_amount(Integer remain_amount) {
        this.remain_amount = remain_amount;
    }

    public Boolean getIs_hidden() {
        return is_hidden;
    }

    public void setIs_hidden(Boolean is_hidden) {
        this.is_hidden = is_hidden;
    }

    public Integer getReward_type() {
        return reward_type;
    }

    public void setReward_type(Integer reward_type) {
        this.reward_type = reward_type;
    }

    public List<Integer> getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(List<Integer> pickup_location) {
        this.pickup_location = pickup_location;
    }
}

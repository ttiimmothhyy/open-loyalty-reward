package com.loyaltyrewards.controllers.admin;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.agent.Agent;
import com.loyaltyrewards.models.form.AgentForm;
import com.loyaltyrewards.models.form.UserForm;
import com.loyaltyrewards.models.user.Gender;
import com.loyaltyrewards.models.user.Role;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.agent.AgentRepository;
import com.loyaltyrewards.repositories.redeem.CartItemRepository;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.RoleRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@RestController
@Component
@RequestMapping("/admin/edit")
public class EditUserController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AgentRepository agentRepository;


    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;



    @GetMapping("/user")
    public ResponseEntity<String> selectAllUser(@RequestParam Integer display, @RequestParam Integer page, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }

        try{
            String sql = String.format("SELECT id,referral_agent_id,date_of_birth,display_name,email,mobile,referral_mobile,username,gender_id,role_id " +
                    "from users " +
                    "LIMIT %1$s OFFSET %2$s", display, display*page-display);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            if (result.size() == 0) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","empty page");
                return ResponseEntity.status(400).body(json.toString());
            }
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<String> selectAllUser(@PathVariable Long id, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }

        try{
            String sql = String.format("SELECT id,referral_agent_id,date_of_birth,display_name,email,mobile,referral_mobile,username,gender_id,role_id,is_deleted " +
                    "from users " +
                    "where id =" +id);
            List<JSONObject> result = this.sqlService.selectQuery2(sql);

            if (result.size() == 0) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","empty page");
                return ResponseEntity.status(400).body(json.toString());
            }
            String sql2 = String.format("SELECT id,each_appointment_commission,each_referral_commission,level,is_deleted,employed_data " +
                    "from agents " +
                    "where user_id =" +id);
            List<JSONObject> result2 = this.sqlService.selectQuery2(sql2);

            JSONObject json=new JSONObject();
            json.put("user",result);
            json.put("agent",result2);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/user/{id}")
    public ResponseEntity<String> updateUser(@PathVariable Long id, @RequestBody UserForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("message", "Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }

        Optional<User> OptionUser = this.userRepository.findById(id);
        if (OptionUser.isPresent()) {
        } else {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("message", "User ID not fond");
            return ResponseEntity.status(400).body(json.toString());
        }
        User user = OptionUser.get();
        try {
            if (form.getPassword().length() > 0) {
                user.setPassword(encoder.encode(form.getPassword()));
            }

            if (form.getRole_id() != null) {
                Role role = roleRepository.findById(form.getRole_id()).get();
                user.setRole(role);
            }

            if (form.getDisplay_name().length() > 0) {
                user.setReferral_mobile(form.getReferral_mobile());
            }
            if (form.getUsername().length() > 0) {
                user.setUsername(form.getUsername());
            }
            if (form.getDisplay_name().length() > 0) {
                user.setDisplay_name(form.getDisplay_name());
            }
            if (form.getEmail().length() > 0) {
                user.setEmail(form.getEmail());
            }
            if (form.getMobile() < 100000000) {
                user.setMobile(form.getMobile());
            } else {
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "mobile error");
                return ResponseEntity.status(400).body(json.toString());
            }
            try {
                if (form.getDate_of_birth().length() > 0) {
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                    Date data_of_birth = format.parse(form.getDate_of_birth());
                    user.setDate_of_birth(data_of_birth);
                }
            } catch (Exception e) {
            }

            if (form.getPassword().length() > 0) {
                user.setPassword(encoder.encode(form.getPassword()));
            }
            if (form.getGender_id() != null) {
                Gender gender = genderRepository.findById(form.getGender_id()).get();
                user.setGender(gender);
            }
            userRepository.save(user);

            JSONObject json = new JSONObject();
            json.put("success", true);
            return ResponseEntity.status(200).body(json.toString());
        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("message", "Edit fail");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

        @PostMapping("/update/agent/{id}")
        public ResponseEntity<String> updateAgent(@PathVariable Long id, @RequestBody AgentForm form, HttpServletRequest request) {
            JWT jwt = decodeJWT.decode(request);
            String checkAdmin = jwt.getLastLogin();

            if (!(checkAdmin.equals("admin"))) {
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "Not a Admin");
                return ResponseEntity.status(400).body(json.toString());
            }

            Optional<Agent> OptionAgent = this.agentRepository.findByUsers(this.userRepository.findById(id).get());
            if (OptionAgent.isPresent()) {
            } else {
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "User ID not fond");
                return ResponseEntity.status(400).body(json.toString());
            }
            Agent agent = OptionAgent.get();
            try {
                agent.setIs_deleted(form.getIs_deleted());
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                Date data_of_birth = format.parse(form.getEmployed_data());
                agent.setEmployed_data(data_of_birth);
                agent.setEach_appointment_commission(form.getEach_appointment_commission());
                agent.setEach_referral_commission(form.getEach_referral_commission());
                agent.setLevel(form.getLevel());
                agentRepository.save(agent);



                JSONObject json = new JSONObject();
                json.put("success", true);
                return ResponseEntity.status(200).body(json.toString());
            } catch (Exception e) {
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "Edit fail");
                return ResponseEntity.status(400).body(json.toString());
            }
        }


    @PostMapping("/create/agent/{id}")
    public ResponseEntity<String> createAgent(@PathVariable Long id, @RequestBody AgentForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("message", "Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }


        Agent agent = new Agent();
        User user;
        try {
            user = userRepository.findById(id).get();
        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("message", "ID not found");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {
            System.out.println("create agent");
            agent.setIs_deleted(form.getIs_deleted());

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Date data_of_birth = format.parse(form.getEmployed_data());
            agent.setEmployed_data(data_of_birth);

            agent.setEach_appointment_commission(form.getEach_appointment_commission());
            agent.setEach_referral_commission(form.getEach_referral_commission());
            agent.setLevel(form.getLevel());
            agent.setNotification(true);
            agent.setUsers(user);
            agentRepository.save(agent);

            JSONObject json = new JSONObject();
            json.put("success", true);
            return ResponseEntity.status(200).body(json.toString());
        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("message", "create fail");
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}

package com.loyaltyrewards.repositories.agent;

import com.loyaltyrewards.models.agent.Result;
import org.springframework.data.repository.CrudRepository;


public interface ResultRepository extends CrudRepository<Result, Long> {

}

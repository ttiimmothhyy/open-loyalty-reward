package com.loyaltyrewards.repositories.user;

import com.loyaltyrewards.models.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.List;
import java.util.UUID;

public interface UserRepository extends CrudRepository<User,Long>{
    Optional<User> findById(Long id);
    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    Optional<User> findByMobile(Long mobile);
    Optional<User> findByUuid(UUID uuid);
    Optional<User> findByOTP(String otp);
	List<User> findAll();
}

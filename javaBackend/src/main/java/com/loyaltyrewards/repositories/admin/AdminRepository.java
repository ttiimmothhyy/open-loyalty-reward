package com.loyaltyrewards.repositories.admin;

import com.loyaltyrewards.models.admin.Admin;
import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<Admin, Long> {
}

import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "reactstrap";
import {IRootState} from "./store";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {push} from "connected-react-router";
import {getAllRewards,getRewardType} from "./redux/rewards/actions";
import "./Reward.scss";

function Reward(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const rewards = useSelector((state:IRootState)=>state.rewards.rewards)
	const [selectPage,setSelectPage] = useState("30");
	const [pageNumber,setPageNumber] = useState(1);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getAllRewards(selectPage,pageNumber))
	},[dispatch,selectPage,pageNumber])

	useEffect(() => {
		dispatch(getRewardType())
	},[dispatch])

	return(
		<div className={sidebar ? "treatment-container-active" : "treatment-container"}>
			<div className="treatment-title-container">
				<div className="treatment-title">所有禮物</div>
				<div className="top-button-container">
					<div className="select-page-another">
					<Button color="secondary" className="add-button" onClick={() => {
						dispatch(push("/create/reward"))
					}}>新增禮物</Button>
					<Button className="arrow-button" onClick={() => {
						pageNumber > 1 && setPageNumber(pageNumber - 1)
					}}><FontAwesomeIcon icon="arrow-left" className="arrow"/></Button>
					<Button className="arrow-right" onClick={() => {
						setPageNumber(pageNumber + 1)
					}}><FontAwesomeIcon icon="arrow-right" className="arrow"/></Button>
					</div>
					<div className="select-page">
						<div>每頁顯示</div>
						<select className="selector" value={selectPage} onChange={(event) => {
							setSelectPage(event.currentTarget.value)
						}}>
							<option value="30">30</option>
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="300">300</option>
						</select>
						<div>項</div>
						<div className="page-number">第{pageNumber}頁</div>
					</div>
				</div>
			</div>
			<div className="flex-treatment-container">
			{rewards.filter(reward=>reward.is_hidden === "false").map((reward,index)=>{
				return(
					<div key={index} className="content-treatment-container"
					style={{backgroundImage:`url(${process.env.REACT_APP_IMAGE_PATH}/rewards/${reward.image})`}}>
						<div className="editor">
							<div className="treatment-content-title">{reward.name}</div>
							<Button color="warning" onClick={() => {
								dispatch(push(`/reward/${reward.id}`))
							}}>編輯禮物</Button>
						</div>
						<div className="content-new-description">禮物內容</div>
						<div className="treatment-detail">{reward.detail}</div>
						<div className="content-new-description">剩餘數量</div>
						<div>{reward.remain_amount}</div>
						<div className="content-new-description">換領所需積分</div>
						<div>{reward.remain_amount}</div>
					</div>
				)
			})}
			</div>
		</div>
	)
}

export default Reward;
import {AppointmentCommission, ReferralCommission, UserPoint} from "../../models";


export interface PointDetail{
	appointment_commission_agent:AppointmentCommission[];
	earned_point_user:UserPoint[];
	referral_commission_agent:ReferralCommission[];
}


export interface AllUserData{
    id:string;
    referral_agent_id:string;
    date_of_birth:string;
    email:string;
    mobile:string;
    referral_mobile:string;
    username:string;
    gender_id:string;
    role_id:string;
    // is_deleted:string;
    display_name:string;
    // password:string;
}
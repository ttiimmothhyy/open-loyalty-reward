import {AdvertisementDetail,AdvertisementSimple,Treatment} from "../../models"
import {IAdvertisementActions} from "./actions"

export interface IAdvertisementState{
    allAdvertisements:AdvertisementSimple[];
    advertisementDetail:AdvertisementDetail;
    createAdvResult:boolean|null;
    updateAdvResult:boolean|null;
	treatment:Treatment[]
}

const initialState = {
    allAdvertisements:[],
    advertisementDetail:{
        image:"",
        advertisements_title:"",
        is_hidden:"",
        id:"",
        detail:"",
        treatments_title:"",
        treatment_id:""
    },
    createAdvResult:null,
    updateAdvResult:null,
	treatment:[]
}


export const advertisementReducer = (state:IAdvertisementState = initialState,action:IAdvertisementActions):IAdvertisementState => {
    switch(action.type){
        case "@@advertisement/Get_all_advertisements":
            return{
                ...state,
                allAdvertisements:action.allAdvertisements
            }
        case "@@advertisement/Get_advertisements_with_id":
			let newAdvertisementDetail = {
				...action.advertisementDetail,
				treatment_id:(state.treatment.filter(treatment=>treatment.title === action.advertisementDetail.treatments_title)
				.map(treatment=>treatment.id))[0]
			}
            return{
                ...state,
                advertisementDetail:newAdvertisementDetail
            }
        case "@@advertisement/Update_advertisement":
            return{
                ...state,
                updateAdvResult:true
            }
        case "@@advertisement/Create_advertisement":
            return{
                ...state,
                createAdvResult:true
            }
        case "@@advertisement/Update_advertisement_title":
            return{
                ...state,
                advertisementDetail:{
                    ...state.advertisementDetail,
                    advertisements_title:action.advTitle
                }
            }
        case "@@advertisement/Update_advertisement_detail":
            return{
                ...state,
                advertisementDetail:{
                    ...state.advertisementDetail,
                    detail:action.advDetail
                }
            }
        case "@@advertisement/Update_advertisement_treatment_id":
            return{
                ...state,
                advertisementDetail:{
                    ...state.advertisementDetail,
                    treatment_id:action.advTreatmentId
                }
            }
        case "@@advertisement/Update_hidden":
            return{
                ...state,
                advertisementDetail:{
                    ...state.advertisementDetail,
                    is_hidden:action.value
                }
            }
        case "@@advertisement/Reset_update_null":
            return{
                ...state,
                updateAdvResult:null
            }
        case "@@advertisement/Reset_create_null":
            return{
                ...state,
                createAdvResult:null
            }
		case "@@advertisement/Update_advertisement_treatment_all_id":
			return{
				...state,
				treatment:action.treatment
			}
    default:
        return state
    }
}
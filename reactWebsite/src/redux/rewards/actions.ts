import {CallHistoryMethodAction, push} from "connected-react-router";
import {Dispatch} from "redux";
import {PlaceCriteria,Reward,RewardCriteria,RewardDetails} from "../../models"

export function getAllRewards(pageDisplay:string,pageNumber:number){
	return async(dispatch:Dispatch<IRewardsActions>) => {
		const token = localStorage.getItem("token");
		// console.log(pageNumber)
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/reward?display=${pageDisplay}\
		&page=${pageNumber}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		if(!result.message){
			dispatch(getAllRewardsSuccess(result))
		}else{
			dispatch(getAllRewardsFailure())
			dispatch(failed("Get_all_rewards_failed",result.message))
		}
	}
}

export function getRewardWithId(id:number|undefined){
	return async(dispatch:Dispatch<IRewardsActions>) => {
		const token = localStorage.getItem("token");
		if(id){
			const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/reward/${id}`,{
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			const result = await res.json();
			// console.log(result)
			if(!result.message){
				dispatch(getRewardWithIdSuccess(result))
			}else{
				dispatch(failed("Get_reward_with_id_failed",result.message))
			}
		}else{
			const rewardDetail = {
				reward:{
					id:"",
					image:"",
					is_hidden:"",
					name:"",
					detail:"",
					remain_amount:"",
					redeem_points:"",
					type:"",
				},
				pickup_location:[],
			}
			dispatch(getRewardWithIdSuccess(rewardDetail))
		}
	}
}

export function getRewardType(){
	return async(dispatch:Dispatch<IRewardsActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/reward/type`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getRewardTypeSuccess(result))
		}else{
			dispatch(failed("Get_reward_type_failed",result.message))
		}
	}
}

export function getPickUpLocation(){
	return async(dispatch:Dispatch<IRewardsActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/reward/location`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getPickUpLocationSuccess(result))
		}else{
			dispatch(failed("Get_pick_up_location_failed",result.message))
		}
	}
}

export function updateReward(rewardId:number,photo:File|null,reward:RewardDetails){
	return async(dispatch:Dispatch<IRewardsActions|CallHistoryMethodAction>) => {
		const token = localStorage.getItem("token");
		let updateFormDate = new FormData();
		if(photo){
			updateFormDate.append("file",photo);
		}
		updateFormDate.append("name",reward.reward.name);
		// updateFormDate.append("default_score",treatment.treatment.default_score);
		updateFormDate.append("detail",reward.reward.detail);
		// updateFormDate.append("is_hidden",treatment.treatment.is_hidden);
		updateFormDate.append("redeem_points",reward.reward.redeem_points);
		updateFormDate.append("remain_amount",reward.reward.remain_amount);
		if(reward.reward.type){
			updateFormDate.append("reward_type",reward.reward.type);
		}
		// console.log(reward.pickup_location.map(place=>parseInt(place.pickup_location_id)).toString())
		updateFormDate.append("pickup_location",reward.pickup_location.map(place=>parseInt(place.pickup_location_id)).toString());

		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/reward/update/${rewardId}`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`
			},
			body:updateFormDate
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(updateRewardSuccess())
			setTimeout(() => {
				dispatch(push("/reward"))
			},2000)
		}else{
			dispatch(updateRewardFailure())
			dispatch(failed("Update_reward_failed",result.message))
		}
	}
}

export function deleteReward(rewardId:number){
	return async(dispatch:Dispatch<IRewardsActions|CallHistoryMethodAction>) => {
		const token = localStorage.getItem("token");
		let updateFormDate = new FormData();
		updateFormDate.append("is_hidden","true");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/reward/update/${rewardId}`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
				// "Content-type":"application/json"
			},
			body:updateFormDate
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(deleteRewardSuccess())
			setTimeout(() => {
				dispatch(push("/reward"))
			},2000)
		}else{
			dispatch(deleteRewardFailure())
			dispatch(failed("Delete_reward_failed",result.message))
		}
	}
}

export function createReward(photo:File|null,reward:RewardDetails){
	return async(dispatch:Dispatch<IRewardsActions|CallHistoryMethodAction>) => {
		const token = localStorage.getItem("token");
		let updateFormDate = new FormData();
		if(photo){
			updateFormDate.append("file",photo);
		}
		if(!reward.reward.name || !reward.reward.detail || !reward.reward.redeem_points || !reward.reward.remain_amount ||
		!reward.reward.is_hidden || !reward.reward.type ||
		reward.pickup_location.map(place=>parseInt(place.pickup_location_id)).length === 0){
			dispatch(createRewardFailure());
			return;
		}

		updateFormDate.append("name",reward.reward.name);
		updateFormDate.append("detail",reward.reward.detail);
		updateFormDate.append("redeem_points",reward.reward.redeem_points);
		updateFormDate.append("remain_amount",reward.reward.remain_amount);
		updateFormDate.append("is_hidden",reward.reward.is_hidden);
		if(reward.reward.type){
			updateFormDate.append("reward_type",reward.reward.type);
		}
		updateFormDate.append("pickup_location",reward.pickup_location.map(location=>parseInt(location.pickup_location_id)).toString());
		// updateFormDate.append("branch",treatment.branch.map(place=>parseInt(place.branch_id)).toString());
		// updateFormDate.append("service",treatment.branch.map(place=>parseInt(place.branch_id)).toString())

		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/reward/create`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`
			},
			body:updateFormDate
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(createRewardSuccess())
			setTimeout(() => {
				dispatch(push("/reward"))
			},2000)
		}else{
			dispatch(createRewardFailure())
			dispatch(failed("Create_reward_failed",result.message))
		}
	}
}

function getAllRewardsSuccess(reward:Reward[]){
	return{
		type:"@@rewards/Get_all_rewards" as const,
		reward
	}
}

function getAllRewardsFailure(){
	return{
		type:"@@rewards/Get_all_rewards_fail" as const,
	}
}

function getRewardWithIdSuccess(reward:RewardDetails){
	return{
		type:"@@rewards/Get_reward_with_id" as const,
		reward
	}
}

export function updateRewardName(name:string){
	return{
		type:"@@rewards/Update_reward_name" as const,
		name,
	}
}

export function updateRewardDetail(detail:string){
	return{
		type:"@@rewards/Update_reward_detail" as const,
		detail
	}
}

export function getRewardTypeSuccess(rewardType:RewardCriteria[]){
	return{
		type:"@@rewards/Get_reward_type" as const,
		rewardType
	}
}

export function updateRewardType(rewardType:string){
	return{
		type:"@@rewards/Update_reward_type" as const,
		rewardType
	}
}

export function getPickUpLocationSuccess(criteria:PlaceCriteria[]){
	return{
		type:"@@rewards/Get_pick_up_location" as const,
		criteria
	}
}

export function updatePickUpLocation(criteria:string,id:string){
	return{
		type:"@@rewards/Update_pick_up_location" as const,
		criteria,
		id
	}
}

export function updateRewardRedeemPoint(point:string){
	return{
		type:"@@rewards/Update_reward_redeem_point" as const,
		point
	}
}

export function updateRewardRemainingAmount(remaining:string){
	return{
		type:"@@rewards/Update_reward_remaining_amount" as const,
		remaining,
	}
}

export function updateRewardHidden(hidden:string){
	return{
		type:"@@rewards/Update_reward_hidden" as const,
		hidden,
	}
}

export function updateRewardSuccess(){
	return{
		type:"@@rewards/Update_reward" as const,
	}
}

export function updateRewardFailure(){
	return{
		type:"@@rewards/Update_reward_fail" as const,
	}
}

export function deleteRewardSuccess(){
	return{
		type:"@@rewards/Delete_reward" as const,
	}
}

export function deleteRewardFailure(){
	return{
		type:"@@rewards/Delete_reward_fail" as const,
	}
}

export function createRewardSuccess(){
	return{
		type:"@@rewards/Create_reward" as const,
	}
}

export function createRewardFailure(){
	return{
		type:"@@rewards/Create_reward_fail" as const,
	}
}

export function resetUpdateNull(){
	return{
		type:"@@rewards/Reset_update_null" as const,
	}
}

export function resetDeleteNull(){
	return{
		type:"@@rewards/Reset_delete_null" as const,
	}
}

export function resetCreateNull(){
	return{
		type:"@@rewards/Reset_create_null" as const,
	}
}

type Failed = "Get_all_rewards_failed"|"Get_reward_with_id_failed"|"Get_reward_type_failed"|"Get_pick_up_location_failed"|
"Update_reward_failed"|"Delete_reward_failed"|"Create_reward_failed"
function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IRewardsActions = ReturnType<typeof getAllRewardsSuccess|typeof getRewardWithIdSuccess|
typeof updateRewardName|typeof updateRewardDetail|typeof getRewardTypeSuccess|typeof updateRewardType|
typeof getPickUpLocationSuccess|typeof updatePickUpLocation|typeof updateRewardRedeemPoint|typeof updateRewardRemainingAmount|
typeof updateRewardHidden|typeof updateRewardSuccess|typeof updateRewardFailure|typeof deleteRewardSuccess|
typeof deleteRewardFailure|typeof createRewardSuccess|typeof createRewardFailure|typeof resetDeleteNull|
typeof resetCreateNull|typeof resetUpdateNull|
typeof getAllRewardsFailure|typeof failed>
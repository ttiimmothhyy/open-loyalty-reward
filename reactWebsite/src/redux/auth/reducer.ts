import {IAuthActions} from "./actions";

export interface IAuthState{
	isAuthenticated:boolean|null;
	isLoggedIn:boolean|null;
	userId:number|null;
	token:string
}

const initialState = {
	isAuthenticated:localStorage.getItem('token') != null,
	isLoggedIn:null,
	userId:null,
	token:"" // no use
}

export const authReducer = (state:IAuthState = initialState,action:IAuthActions):IAuthState => {
	switch(action.type){
		case "@@auth/Get_user_data":
			return{
				...state,
			}
		case "@@auth/Login_success":
			return{
				...state,
				isLoggedIn:true,
				isAuthenticated:true,
				userId:action.id,
				token:action.token
			}
		case "@@auth/Login_fail":
			return{
				...state,
				isLoggedIn:false,
				isAuthenticated:false
			}
		case "@@auth/Logout":
			return{
				...state,
				isAuthenticated:false,
				userId:null,
				token:""
			}
		case "@@auth/Reset_authenticated_null":
			return{
				...state,
				isAuthenticated:null
			}
		case "@@auth/Reset_logged_in_null":
			return{
				...state,
				isLoggedIn:null
			}
		default:
			return state
	}
}
import {push} from "connected-react-router";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {Button,Input} from "reactstrap";
import {requestResetLink,resetIsRequestedNull,resetNull,resetPassword} from "./redux/resetPassword/actions";
import {IRootState} from "./store";
import "./ResetPassword.scss";

function ResetPassword() {
    const sidebar = useSelector((state: IRootState) => state.sidebar.sidebarToggle);
    const params = useParams<{uuid: string}>();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [wrongEmail, setWrongEmail] = useState(false);
    const dispatch = useDispatch();
    const isReset = useSelector((state: IRootState) => state.resetPassword.isReset);
    const isRequested = useSelector((state: IRootState) => state.resetPassword.isRequested);
    // console.log(params)
    useEffect(() => {
        if(isReset) {
            setTimeout(() => {
                dispatch(push("/"));
            },3000);
        }
    },[isReset,dispatch]);

	useEffect(() => {
		dispatch(resetIsRequestedNull());
	},[dispatch])

    return (
        <div className={sidebar ? "treatment-container-active" : "treatment-container"}>
            <div>
                <div className="treatment-title">重設密碼</div>
                {/* content container */}
                {!params.uuid && (
                    <div>
                        <div className="flex-advertisement-detail-content">
                            <div className="advertisement-input-content-detail-short">電郵:</div>
                            <Input className="advertisement-detail-title-fix" value={email} onChange={(event) => {
								setEmail(event.currentTarget.value);
								setWrongEmail(false);
								// dispatch(resetIsRequestedNull());
							}}/>
                        </div>

                        {/* buttons container */}
                        <div>
                            {/* back to login page button */}
                            <Button className="button-boundary" outline color="warning" onClick={() => {
								dispatch(push(`/`));
								setWrongEmail(false);
								dispatch(resetIsRequestedNull());
							}}>
                                返回登入頁面
                            </Button>

                            {/* send button */}
                            <Button className="button-boundary" outline color="warning" onClick={() => {
								// wrong email if no @
								!email.includes("@") && setWrongEmail(true);
								// 有@then dispatch
								dispatch(requestResetLink(email));
								// console.log(isRequested + " after");
							}}>
                                傳送
                            </Button>
                        </div>
                        {/* status container */}
                        <div>
                            {/* reset ed */}
                            {isRequested && <div className="notification">已傳送</div>}
                            {wrongEmail && <div className="warning">這電郵不正確</div>}
                            {/* this username has not been registered */}
                            {isRequested === false && <div className="warning">此電郵沒有登記資料</div>}
                        </div>
                    </div>
                )}
                {/* content container */}
                {params.uuid && (
                    <div>
                        <div className="flex-advertisement-detail-content">
                            <div className="advertisement-input-content-detail-short">密碼:</div>
                            <Input className="advertisement-detail-title-fix" value={password}
							onChange={(event) => {
								setPassword(event.currentTarget.value);
								// setWrongUsername(false);
								// dispatch(resetNull());
							}}/>
                        </div>

                        {/* buttons container */}
                        <div>
                            {/* back to login page button */}
                            <Button className="button-boundary" outline color="warning" onClick={() => {
								dispatch(push(`/`));
								// setWrongUsername(false);
								dispatch(resetNull());
							}}>
                                返回登入頁面
                            </Button>
                            {/* send button */}
                            <Button className="button-boundary" outline color="warning" onClick={() => {
								// wrong email if no @
								// {!email.includes("@") && setWrongEmail(true)};
								// 有@then dispatch
								dispatch(resetPassword(params.uuid, password));
								// console.log(isReset);
							}}>
								傳送
                            </Button>
                        </div>
                        {/* status container */}
                        <div>
                            {/* reset ed */}
                            {isReset && <div className="notification">已傳送</div>}
                            {/* wrong username
                        	{wrongUsername && (
								<div className="warning">這用戶名稱不正確</div>
							)} */}
                            {/* this username has not been registered */}
                            {isReset === false && <div className="warning">更改密碼未成功</div>}
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}

export default ResetPassword;

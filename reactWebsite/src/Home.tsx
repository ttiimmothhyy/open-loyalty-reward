import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {IRootState} from "./store";
import "./Home.scss";
import {Button} from "reactstrap";
import {push} from "connected-react-router";

function Home(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const dispatch = useDispatch();
	return(
		<div className={sidebar ? "home-container-active" : "home-container"}>
			<Button color="#ffffff" className="content-container" onClick={() => {
				dispatch(push("/editUser"))
			}}>
				<div className="content-title">編輯客戶</div>
				<div className="content-description">修改客戶資料和批准客戶成為中介</div>
			</Button>
			<Button color="#ffffff" className="content-container" onClick={() => {
				dispatch(push("/treatment"))
			}}>
				<div className="content-title" >編輯療程</div>
				<div className="content-description">新增和刪減療程以及更改療程介紹</div>
			</Button>
			<Button color="#ffffff" className="content-container" onClick={() => {
				dispatch(push("/reward"))
			}}>
				<div className="content-title">編輯禮物</div>
				<div className="content-description">新增和刪減療程以及更改禮物介紹和內容</div>
			</Button>
			<Button color="#ffffff" className="content-container" onClick={() => {
				dispatch(push("/advertisement"))
			}}>
				<div className="content-title">編輯廣告</div>
				<div className="content-description">新增和刪減療程以及更改應用程式首頁的廣告</div>
			</Button>
			<Button color="#ffffff" className="content-container" onClick={() => {
				dispatch(push("/point"))
			}}>
				<div className="content-title">輸入積分</div>
				<div className="content-description">手動輸入客戶積分</div>
			</Button>
		</div>
	)
}

export default Home;
import React,{useEffect,useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {Button,Input} from "reactstrap";
import {getPickUpLocation,getRewardType,getRewardWithId,updatePickUpLocation,updateReward,updateRewardDetail,updateRewardHidden,
updateRewardName,updateRewardRedeemPoint,updateRewardRemainingAmount,updateRewardType,
resetCreateNull,resetDeleteNull,resetUpdateNull,deleteReward,createReward}
from "./redux/rewards/actions";
import {IRootState} from "./store";
import "./RewardDetail.scss"

function upload(){
	document.getElementById("file-upload")?.click();
}


function RewardDetail(){
	let purchaseAmount:number[] = []
	for(let i = 1; i <= 30; i++){
		purchaseAmount.push(i)
	}

	// const fileUpload = useRef<HTMLInputElement|null>(null);

	// useEffect(()=>{
	// 	if(fileUpload.current){
	// 		(fileUpload.current as HTMLInputElement).click()
	// 	}
	// },[fileUpload.current])
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const params = useParams<{id:string}>();
	// const treatment = useSelector((state:IRootState)=>state.treatment.treatmentDetail);
	const reward = useSelector((state:IRootState)=>state.rewards.rewardDetail);
	const [photo,setPhoto] = useState<File|null>(null)
	const [preview,setPreview] = useState<string|undefined>();
	const [selectedFile,setSelectedFile] = useState();
	const rewardType = useSelector((state:IRootState)=>state.rewards.rewardType)
	const pickUpCriteria = useSelector((state:IRootState)=>state.rewards.pickUpCriteria)
	const updateSuccess = useSelector((state:IRootState)=>state.rewards.update)
	const deleteSuccess = useSelector((state:IRootState)=>state.rewards.delete)
	const createSuccess = useSelector((state:IRootState)=>state.rewards.create)
	const dispatch = useDispatch();
	// console.log(reward.reward.type)
	// console.log(treatment.treatment.is_hidden)
	// console.log(photo)

	// const criteriaGender = [{id:1,gender:"男性"},{id:2,gender:"女性"}]
	// console.log(params)
	useEffect(() => {
		dispatch(getRewardWithId(parseInt(params.id)))
		dispatch(getRewardType())
		dispatch(getPickUpLocation())
	},[dispatch,params.id])

    // create a preview as a side effect, whenever selected file is changed
    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }
        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)

        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

	useEffect(() => {
		dispatch(resetUpdateNull())
		dispatch(resetDeleteNull())
		dispatch(resetCreateNull())
	},[dispatch])
	// console.log(treatment)
	return(
		<div className={sidebar ? "treatment-container-active" : "treatment-container"}>
			<div className="flex-treatment-detail-content">
				<div className="treatment-content-detail-same">禮物名稱</div>
				<Input className="treatment-detail-title"
				value={reward.reward.name}
				onChange={(event) => {
					dispatch(updateRewardName(event.currentTarget.value))
				}}/>
			</div>
			<div className="treatment-content-detail">禮物內容</div>
			<Input className="treatment-detail-description" value={reward?.reward.detail}
			onChange={(event) => {
				dispatch(updateRewardDetail(event.currentTarget.value))
			}}/>
			<div className="treatment-content-detail-no-width">禮物相關相片</div>
			<div className="image-container">
				<div className="treatment-image-container">
					<img src={selectedFile ? preview : `${process.env.REACT_APP_IMAGE_PATH}/rewards/${reward?.reward.image}`}
					alt="" className="treatment-image"/>
				</div>
				<Button onClick={() => {upload()}}>更改圖片</Button>
				<input id="file-upload" /*ref={fileUpload}*/ type="file" hidden onChange={(event:any) => {
					let files = event.currentTarget.files
					// console.log(files)
					if(files){
						setPhoto(files[0])
						setSelectedFile(files[0])
					}
					// previewImage(event)
				}}/>
			</div>
			<div className="flex-treatment-detail-content">
				<div className="treatment-content-detail-no-width-same">禮物類型</div>
				<select value={reward.reward.type} onChange={(event) => {
					dispatch(updateRewardType(event.currentTarget.value))
				}}>
					<option value=""></option>
					{rewardType.map((type,index)=>
						(<option key={index} value={type.id}>{type.type}</option>)
					)}
				</select>
			</div>
			<div className="treatment-content-detail-no-width">禮物可領取地點</div>
			<div className="criteria-container">
				{pickUpCriteria.map((criteria)=>
					{
						return(
							<div key={criteria.id} className="criteria">
								<Input type="checkbox" value={criteria.name} checked={reward.pickup_location.some
								(location=>location.pickup_location_id === criteria.id)}
								onChange={() => {
									dispatch(updatePickUpLocation(criteria.name,criteria.id))
								}}/>
								<div className="criteria-name">{criteria.name}</div>
							</div>
						)
					}
				)}
			</div>
			<div className="flex-treatment-detail-content">
				<div className="treatment-content-detail-no-width-same">換取禮物所需要積分</div>
				<div className="small-input-container">
					<Input className="treatment-detail-title" value={reward?.reward.redeem_points}
					onChange={(event) => {
						dispatch(updateRewardRedeemPoint(event.currentTarget.value))
					}}/>
				</div>
			</div>
			<div className="flex-treatment-detail-content">
				<div className="treatment-content-detail-no-width-same">禮物剩餘數量</div>
				<select className="treatment-detail-title-select" value={reward?.reward.remain_amount}
				onChange={(event) => {
					dispatch(updateRewardRemainingAmount(event.currentTarget.value))
				}}>
					<option value=""></option>
					{purchaseAmount.map(value=>(
						<option key={value} value={value}>{value}</option>
					))}
				</select>
			</div>
			{!params.id &&
				<div className="flex-treatment-detail-content">
					<div className="treatment-content-detail-no-width">隱藏禮物</div>
					<select className="treatment-detail-title" value={reward?.reward.is_hidden}
					onChange={(event) => {
						dispatch(updateRewardHidden(event.currentTarget.value))
					}}>
						<option value=""></option>
						<option value="true">是</option>
						<option value="false">否</option>
					</select>
				</div>
			}
			{
				params.id &&
				<div>
					<Button color="success" className="change-button" onClick={() => {
						dispatch(updateReward(parseInt(params.id),photo,reward))
					}}>更改禮物內容</Button>
					<Button color="info" className="delete-button" onClick={() => {
						dispatch(deleteReward(parseInt(params.id)))
					}}>隱藏禮物</Button>
					{updateSuccess ? <div className="notification">更改成功</div> : updateSuccess === false &&
					<div className="warning">更改失敗</div>}
					{deleteSuccess ? <div className="notification">隠藏成功</div> : deleteSuccess === false &&
					<div className="warning">隠藏失敗</div>}
				</div>
			}
			{
				!params.id &&
				<div>
					<Button color="success" className="change-button" onClick={() => {
						dispatch(createReward(photo,reward))
					}}>新增療程</Button>
					{/* <Button color="info" className="delete-button">隱藏療程</Button> */}
					{createSuccess ? <div className="notification">新增成功</div> : createSuccess === false &&
					<div className="long-warning">未填寫所有資料/未選擇是否顯示/系統故障</div>}
				</div>
			}
		</div>
	)
}

export default RewardDetail;
import React,{useEffect,useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {Button,Input} from "reactstrap";
// import {TreatmentDetails} from "./models";
import {changeGender,changePlace,changeType,createTreatment,deleteTreatment,getTreatmentWithId,placeCriteria,
resetCreateNull,resetDeleteNull,resetUpdateNull,typeCriteria,updateHidden,updateTreatment,
updateTreatmentBenefit,updateTreatmentDetail,updateTreatmentName,updateTreatmentScore,updateTreatmentValue}
from "./redux/treatment/actions";
import {IRootState} from "./store";
import "./TreatmentDetail.scss"

function upload(){
	document.getElementById("file-upload")?.click();
}

let purchaseAmount:number[] = []
for(let i = 1; i <= 20; i++){
	purchaseAmount.push(i)
}


// function TreatmentDetailLoading(){
// 	const params = useParams<{id:string}>();
// 	const dispatch = useDispatch();

// 	const treatmentDetail  = useSelector((state:IRootState)=>state.treatment.treatmentDetail);

// 	useEffect(() => {
// 		dispatch(getTreatmentWithId(parseInt(params.id)))
// 		dispatch(typeCriteria())
// 		dispatch(placeCriteria())
// 	},[dispatch,params.id]);

// 	if(treatmentDetail.treatment.id === ""){
// 		return <div>Loading...</div>;
// 	}else{
// 		return <TreatmentDetail treatmentDetail={treatmentDetail}/>
// 	}
// }

// interface ITreatmentDetailProps{
// 	treatmentDetail:TreatmentDetails
// }

function TreatmentDetail(){

	// can use local state
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const params = useParams<{id:string}>();
	const treatment = useSelector((state:IRootState)=>state.treatment.treatmentDetail);
	const [photo,setPhoto] = useState<File|null>(null)
	const [preview,setPreview] = useState<string|undefined>();
	const [selectedFile,setSelectedFile] = useState();
	const criteriaType = useSelector((state:IRootState)=>state.treatment.typeCriteria)
	const criteriaPlace = useSelector((state:IRootState)=>state.treatment.placeCriteria)
	const updateSuccess = useSelector((state:IRootState)=>state.treatment.update)
	const deleteSuccess = useSelector((state:IRootState)=>state.treatment.delete)
	const createSuccess = useSelector((state:IRootState)=>state.treatment.create)
	const dispatch = useDispatch();
	// console.log(treatment.treatment.is_hidden)
	// console.log(photo)

	const criteriaGender = [{id:1,gender:"男性"},{id:2,gender:"女性"}]
	// console.log(params)
	useEffect(() => {
		dispatch(getTreatmentWithId(parseInt(params.id)))
		dispatch(typeCriteria())
		dispatch(placeCriteria())
	},[dispatch,params.id])

    // create a preview as a side effect, whenever selected file is changed
    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }
        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)

        // free memory when ever this component is unmounted
		// Good usage
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

	// this problem goes away if you are using local state
	useEffect(() => {
		dispatch(resetUpdateNull())
		dispatch(resetDeleteNull())
		dispatch(resetCreateNull())
	},[dispatch])
	// console.log(treatment)
	return(
		<div className={sidebar ? "treatment-container-active" : "treatment-container"}>
			<div className="flex-treatment-detail-content">
				<div className="treatment-content-detail-same">療程名稱</div>
				<Input className="treatment-detail-title"
				value={treatment.treatment.title}
				onChange={(event) => {
					dispatch(updateTreatmentName(event.currentTarget.value))
				}}/>
			</div>
			<div className="treatment-content-detail">療程內容</div>
			<Input className="treatment-detail-description" value={treatment?.treatment.detail}
			onChange={(event) => {
				dispatch(updateTreatmentDetail(event.currentTarget.value))
			}}/>
			<div className="treatment-content-detail-no-width">禮物相關相片</div>
			<div className="image-container">
			<div className="treatment-image-container">
				<img src={selectedFile ? preview : `${process.env.REACT_APP_IMAGE_PATH}/treatments/${treatment?.treatment.image}`}
				alt="" className="treatment-image"/>
			</div>
			<Button onClick={() => {upload()}}>更改圖片</Button>
			<input id="file-upload" type="file" hidden onChange={(event:any) => {
				let files = event.currentTarget.files
				// console.log(files)
				if(files){
					setPhoto(files[0])
					setSelectedFile(files[0])
				}
				// previewImage(event)
			}}/>
			</div>
			<div className="flex-treatment-detail-content">
				<div className="treatment-content-detail-no-width-same">療程提供積分</div>
				<div className="small-input-container">
					<Input className="treatment-detail-title" value={treatment?.treatment.default_score}
					onChange={(event) => {
						dispatch(updateTreatmentScore(event.currentTarget.value))
					}}/>
				</div>
			</div>
			<div className="flex-treatment-detail-content">
				<div className="treatment-content-detail-no-width-same">療程賺買優惠</div>
				<div className="small-selection-container">
					<div className="buy-word">買</div>
					<select className="treatment-detail-title" value={treatment?.treatment.purchase_benefit_condition}
					onChange={(event) => {
						dispatch(updateTreatmentValue(event.currentTarget.value))
					}}>
						<option value=""></option>
						{purchaseAmount.map(value=>(
							<option key={value} value={value}>{value}</option>
						))}
					</select>
					<div className="buy-word">送</div>
					<select className="treatment-detail-title" value={treatment?.treatment.purchase_benefit_amount}
					onChange={(event) => {
						dispatch(updateTreatmentBenefit(event.currentTarget.value))
					}}>
						<option value=""></option>
						{purchaseAmount.map(value=>(
							<option key={value} value={value}>{value}</option>
						))}
					</select>
				</div>
			</div>
			<div className="treatment-content-detail-no-width">所屬類別</div>
			<div className="criteria-container">
				{criteriaType.map((criteria)=>
					{return(
						<div key={criteria.id} className="criteria">
							<Input type="checkbox" value={criteria.service} checked={treatment.service.some
							(service=>service.service_id === criteria.id)}
							onChange={(event) => {
								dispatch(changeType(event.currentTarget.value,criteria.id))
							}}/>
							<div className="criteria-name">{criteria.service}</div>
						</div>
					)}
				)}
			</div>
			<div className="treatment-content-detail-no-width">療程性別向</div>
			<div className="criteria-container">
				{criteriaGender.map((criteria)=>
					{
						return(
							<div key={criteria.id} className="criteria">
								<Input type="checkbox" value={criteria.gender} checked={treatment.gender.some
								(gender=>parseInt(gender.gender_id) === criteria.id)}
								onChange={() => {
									dispatch(changeGender(criteria.id + ""))
								}}/>
								<div className="criteria-name">{criteria.gender}</div>
							</div>
						)
					}
				)}
			</div>
			<div className="treatment-content-detail-no-width">可進行療程的分店</div>
			<div className="criteria-container">
				{criteriaPlace.map((criteria)=>
					{
						return(
							<div key={criteria.id} className="criteria">
								<Input type="checkbox" value={criteria.name} checked={treatment.branch.some
								(place=>place.branch_id === criteria.id)}
								onChange={(event) => {
									dispatch(changePlace(event.currentTarget.value,criteria.id))
								}}/>
								<div className="criteria-name">{criteria.name}</div>
							</div>
						)
					}
				)}
			</div>
			{!params.id &&
				<div className="flex-treatment-detail-content">
					<div className="treatment-content-detail-no-width">隱藏療程</div>
					<select className="treatment-detail-title" value={treatment?.treatment.is_hidden}
					onChange={(event) => {
						dispatch(updateHidden(event.currentTarget.value))
					}}>
						<option value=""></option>
						<option value="true">是</option>
						<option value="false">否</option>
					</select>
				</div>
			}
			{
				params.id &&
				<div>
					<Button color="success" className="change-button" onClick={() => {
						dispatch(updateTreatment(parseInt(params.id),photo,treatment))
					}}>更改療程</Button>
					<Button color="info" className="delete-button" onClick={() => {
						dispatch(deleteTreatment(parseInt(params.id)))
					}}>隱藏療程</Button>
					{updateSuccess ? <div className="notification">更改成功</div> : updateSuccess === false &&
					<div className="warning">更改失敗</div>}
					{deleteSuccess ? <div className="notification">隠藏成功</div> : deleteSuccess === false &&
					<div className="warning">隠藏失敗</div>}
				</div>
			}
			{
				!params.id &&
				<div>
					<Button color="success" className="change-button" onClick={() => {
						dispatch(createTreatment(photo,treatment))
					}}>新增療程</Button>
					{/* <Button color="info" className="delete-button">隱藏療程</Button> */}
					{createSuccess ? <div className="notification">新增成功</div> : createSuccess === false &&
					<div className="long-warning">未填寫所有資料/未選擇是否顯示/系統故障</div>}
				</div>
			}
		</div>
		// <div></div>
	)
}

export default TreatmentDetail;